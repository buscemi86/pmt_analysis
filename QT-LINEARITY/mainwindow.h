#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QDebug>
#include <QTime>
#include "mk_list.h"
#include "linearity.h"
#include "single_histo.h"
#include "fill_db.h"
#include "write_pmt.h"
#include "configuration.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


int loadDataFiles()
{ // ui->chooseButton->setEnabled(true);
         Directory = QFileDialog::getExistingDirectory(this,
                        tr("Choose File Directory"),
                        "../Data",
                       QFileDialog::DontResolveSymlinks);
    ui->textEdit_20->clear();
//if(!Directory.isEmpty())
ui->textEdit_20->append( Directory);

if(Directory.contains("NL_L", Qt::CaseInsensitive)) flag_lowgain=1;
else if(Directory.contains("NL_H", Qt::CaseInsensitive)) flag_lowgain=0;
else if(Directory.contains("NL_LL", Qt::CaseInsensitive)) flag_lowgain=1;
else if(Directory.contains("NL_HH", Qt::CaseInsensitive)) flag_lowgain=0;

int pos1=Directory.indexOf("RUN");
RunNumber=Directory.mid(pos1,7);

return flag_lowgain;
}

QString getDirectory(){return Directory;}
QString getRun(){return RunNumber;}
private slots:

    void on_chooseButton_clicked();

    void on_analyzeButton_clicked();

    void on_drawAllbutton_clicked();

    void on_drawButton_clicked();

    void on_choose_histo_clicked();

    void on_analyze_histo_clicked();

//    void on_checkBox_2_stateChanged(int arg1);

    void on_checkBox_db0_stateChanged(int arg1);

    void on_checkBox_db1_stateChanged(int arg1);

    void on_checkBox_db2_stateChanged(int arg1);

    void on_checkBox_db3_stateChanged(int arg1);

    void on_checkBox_db4_stateChanged(int arg1);

    void on_checkBox_db5_stateChanged(int arg1);

    void on_checkBox_db6_stateChanged(int arg1);

    void on_checkBox_db7_stateChanged(int arg1);

    void on_checkBox_db8_stateChanged(int arg1);

    void on_checkBox_db9_stateChanged(int arg1);

    void on_checkBox_db10_stateChanged(int arg1);

    void on_checkBox_db11_stateChanged(int arg1);

    void on_checkBox_db12_stateChanged(int arg1);

    void on_checkBox_db13_stateChanged(int arg1);

    void on_checkBox_db14_stateChanged(int arg1);

    void on_checkBox_db15_stateChanged(int arg1);

    void on_clearButton_clicked();

    void on_connectButton_clicked();


    void on_storeAll_Button_clicked();

    void on_uploadButton_clicked();

    void on_disconnectButton_clicked();

    void on_storeButton_clicked();



private:
    Ui::MainWindow *ui;
    mk_list* list;
    single_histo* histogram;
    fill_db* db;
    write_pmt* pmt_data;

    vector<QString> ID;
    QString Directory , RunNumber;
    vector<double> linConfig;
    int flag_spe;
    int flag_db;
    int flag_lowgain;
//    vector<vector<double>> lin_value;
    vector<double> lin_value;
 configuration* config;
void setPMTcurveID(QString ID){gainID=ID;}
void delay()
{
    QTime dieTime= QTime::currentTime().addSecs(3);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

QString gainID;
QString  fileName;
int status_list , status_an , status_histo;

int flag_db0=0  , flag_db1=0   , flag_db2=0  , flag_db3=0;
int flag_db4=0  , flag_db5=0   , flag_db6=0  , flag_db7=0;
int flag_db8=0  , flag_db9=0   , flag_db10=0 , flag_db11=0;
int flag_db12=0 , flag_db13=0  , flag_db14=0 , flag_db15=0;


};

#endif // MAINWINDOW_H
