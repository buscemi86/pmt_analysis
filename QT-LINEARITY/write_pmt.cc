#include "write_pmt.h"


void write_pmt::analyze(vector<vector<QFileInfo>> vec){


list=vec;

read_file* read_data = new read_file();
vector<vector<double>> list_vec_data;
for(unsigned int i=0;i<list.size();i++){
list_vec_data.clear();
vector<double> vec_data;
	for(unsigned int j=0;j<list.at(i).size();j++){
		vec_data.clear();
		cout<<list.at(i).at(j).filePath().toStdString()<<"  ";
		int flag;
		if(list.at(i).at(j).fileName().contains("spe", Qt::CaseInsensitive)==true) flag=2;
		else flag=1;
cout<<flag<<endl;

		read_data->read(list.at(i).at(j),flag);
		double ampl_1=read_data->get_mean_A1();
		double sigmaA_1=read_data->get_sigma_A1();
		double charge_1=read_data->get_mean_Q1();
		double sigmaQ_1=read_data->get_sigma_Q1();
		double ampl_2=read_data->get_mean_A2();
		double sigmaA_2=read_data->get_sigma_A2();
		double charge_2=read_data->get_mean_Q2();
		double sigmaQ_2=read_data->get_sigma_Q2();
		double mean_ratio=read_data->get_mean_r();
		double sigma_ratio=read_data->get_sigma_r();
//cout<<"*** "<<ampl<<"\t"<<sigmaA<<"\t"<<charge<<"\t"<<sigmaQ<<endl;

		vec_data.push_back(ampl_1*1000); // in mV
		vec_data.push_back(sigmaA_1*1000); // in mV
		vec_data.push_back(charge_1);
		vec_data.push_back(sigmaQ_1);
		vec_data.push_back(ampl_2*1000); // in mV
		vec_data.push_back(sigmaA_2*1000); // in mV
		vec_data.push_back(charge_2);
		vec_data.push_back(sigmaQ_2);		
		vec_data.push_back(mean_ratio);
		vec_data.push_back(sigma_ratio);		

		list_vec_data.push_back(vec_data);
	
							}
list_all_data.push_back(list_vec_data);
					}	
delete read_data;
//cout<<list_all_data.at(1).at(1).size()<<" "<<list_all_data.size()<<" "<<list_all_data.at(1).size()<<endl;		
}

///////////////
int write_pmt::save_file(vector<QString> ID , int flag_lowgain){

//cout<<"ID size = "<<ID.size()<<" "<<list_all_data.at(1).at(1).size()<<" "<<list_all_data.size()<<" "<<list_all_data.at(1).size()<<endl;
const char* configfile ="config.dat";

QFile config(configfile);
QTextStream in(&config);
QString tmp, config_path , path1 , path2;
if(!config.open(QIODevice::ReadOnly | QIODevice::Text)) cout<<"config file not found!"<<endl;

tmp = in.readLine();
path1 = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
path2 = in.readLine();




if (flag_lowgain == 0){

config_path=path1;
}
else{
config_path=path2;
}



//for(unsigned int i=0;i<ID.size();i++){
for(unsigned int i=0;i<list_all_data.size();i++){

QString path=config_path;
QString index=ID[i];         
path.append("/");
path.append(index);
QDir dir;
//if(!dir.exists())
dir.mkpath(path); 
path.append("/lin_output.txt");
string path_str = path.toStdString();
ofstream myfile;
  	myfile.open (path_str.c_str());
myfile<<index.toStdString()<<internal<<setw(18) <<"Amp1 (mV) "<<internal<<setw(18)<<"sigma Amp1"<<internal<< setw(18)<<"Q1 (pVs)"<< setw(18)<<internal<<"sigma Q1"<<internal<<setw(18) <<"Amp2(mV) "<<internal<<setw(18)<<"sigma Amp2"<<internal<< setw(18)<<"Q2 (pVs)"<< setw(18)<<internal<<"sigma Q2"<<setw(18)<<internal<<"ratio"<< setw(18)<<internal<<"sigma ratio"<< setw(18)<<endl;
	for(unsigned int j=0; j<list_all_data.at(i).size();j++) {
		
		for(int k=0;k<10;k++)  {
			myfile<<setiosflags(ios::fixed)<<setprecision(2)<<internal<<setw(18)<<fabs(list_all_data.at(i).at(j).at(k));}
			myfile <<endl;
//for(int k=0;k<4;k++) cout<<setiosflags(ios::fixed)<<setprecision(2)<<internal<<fabs(list_all_data.at(i).at(j).at(k))<<setw(18);
	//	cout<<endl;


}


 myfile.close(); 

}
return 0;
}









