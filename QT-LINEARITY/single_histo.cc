#include "single_histo.h"

int single_histo::histo(string file_data, int f){


flag_fit=f;

double a , b, c, d;
TFile* out = new TFile("out.root","recreate");
//fileName = files.filePath().toStdString();
//cout<<"reading file: "<<files.filePath().toStdString()<<endl;
 
ifstream file (file_data.c_str());

TF1*   fgaus_q = new TF1("fgaus_a","gaus",-100,20000); 
TF1*   fgaus_a = new TF1("fgaus_q","gaus",0,1);
TF1*   fgaus_ratio = new TF1("fgaus_r","gaus",4,7);
Double_t par[6];
 
double deltaA =0.03;
double deltaC = 250;
int binmax = 0;
double xmax=0;

TH1D* t1=new TH1D("t1","Ampl1",1024,0,1);
TH1D* t2=new TH1D("t2","Charge1",1500,0,30000);
TH1D* t3=new TH1D("t3","Ampl2",1024,0,1);
TH1D* t4=new TH1D("t4","Charge2",1500,0,30000);
TH1D* t5=new TH1D("t5","ratio",1000,0.,10);
t1->GetXaxis()->SetTitle("Peak Ampl [V]");
t1->GetYaxis()->SetTitle("counts");

t2->GetXaxis()->SetTitle("Charge Integral [pVs]");
t2->GetYaxis()->SetTitle("counts");

t3->GetXaxis()->SetTitle("Peak Ampl [V]");
t3->GetYaxis()->SetTitle("counts");

t4->GetXaxis()->SetTitle("Charge Integral [pVs] [V]");
t4->GetYaxis()->SetTitle("counts");

t5->GetXaxis()->SetTitle("charge ratio");
t5->GetYaxis()->SetTitle("counts");

while(file>>a>>b>>c>>d)
{

t1->Fill(fabs(a));
t2->Fill(fabs(b));
t3->Fill(fabs(c));
t4->Fill(fabs(d));
t5->Fill(fabs(b/d));
}
file.close();

t1->Rebin(2);
binmax =t1->GetMaximumBin();
xmax=t1->GetXaxis()->GetBinCenter(binmax);
t1->Fit("gaus","","",xmax-deltaA,xmax+deltaA);
TF1 *fit1 = t1->GetFunction("gaus");
mean_A1 = fit1->GetParameter(1);
sigma_A1 = fit1->GetParameter(2);

t2->Rebin(2);
binmax =t2->GetMaximumBin();
xmax=t2->GetXaxis()->GetBinCenter(binmax);
t2->Fit("gaus","","",xmax-deltaC,xmax+deltaC);
TF1 *fit2 = t2->GetFunction("gaus");
mean_Q1 = fit2->GetParameter(1);
sigma_Q1 = fit2->GetParameter(2);

t3->Rebin(2);
binmax =t3->GetMaximumBin();
xmax=t3->GetXaxis()->GetBinCenter(binmax);
t3->Fit("gaus","","",xmax-deltaA,xmax+deltaA);
TF1 *fit3 = t3->GetFunction("gaus");
mean_A2 = fit3->GetParameter(1);
sigma_A2 = fit3->GetParameter(2);

t4->Rebin(2);
binmax =t4->GetMaximumBin();
xmax=t4->GetXaxis()->GetBinCenter(binmax);
t4->Fit("gaus","","",xmax-deltaC,xmax+deltaC);
TF1 *fit4 = t4->GetFunction("gaus");
mean_Q2 = fit4->GetParameter(1);
sigma_Q2 = fit4->GetParameter(2);

t5->Rebin(2);
binmax =t5->GetMaximumBin();
xmax=t5->GetXaxis()->GetBinCenter(binmax);
t5->Fit("gaus","","",xmax-2,xmax+2);
//t5->Fit("gaus","","",3,7);
TF1 *fit5= t5->GetFunction("gaus");
mean_r = fit5->GetParameter(1);
sigma_r = fit5->GetParameter(2);

t1->Draw();
t2->Draw();
t3->Draw();
t4->Draw();
t5->Draw();

t1->Write();
t2->Write();
t3->Write();
t4->Write();
t5->Write();

out->Write();
out->Close();

return 0;
}

