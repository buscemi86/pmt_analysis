#ifndef LINEARITY_H
#define LINEARITY_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <iomanip>  


#include <TMath.h>
#include <TFile.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TFrame.h>
#include <TGraphErrors.h>
#include <TLegend.h>
#include <TF1.h>
#include <TFitResult.h>
#include <QString>
using namespace std;

class linearity{
public:
 linearity ( QString , int);
double get_value(){return nl5;}
//void setSpeConfig(vector<int> spe);
//void setGainConfig(vector<int> gain);
private:
vector<double> value;
double nl5;

};


#endif
