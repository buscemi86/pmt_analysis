#include "fill_db.h"
#include <QFile>
#include <QMenu>
#include <fstream>
#include <QDebug>
#include <QSqlQuery>

fill_db::fill_db(Ui::MainWindow *ui):
ui_(ui)
{

}

void fill_db::show_data(vector<QString> ID ,vector<double> lin_value, int flag_lowgain , QString RunNumber){
IDpmt=ID;
RUN=RunNumber.mid(4,3);
flag_GAIN=flag_lowgain;

//set pmt0 lin value
ui_->textEdit_d01->append(ID[0]);
if(flag_GAIN==0){
ui_->textEdit_d02->append(QString::number(lin_value[0],'g',3));
db0.clear();
db0.push_back(ui_->textEdit_d02->toPlainText().toDouble());
}
else{
ui_->textEdit_d03->append(QString::number(lin_value[0],'g',3));
db0.clear();
db0.push_back(ui_->textEdit_d03->toPlainText().toDouble());}

//set pmt1 gain value
ui_->textEdit_d11->append(ID[1]);
if(flag_GAIN==0){
ui_->textEdit_d12->append(QString::number(lin_value[1],'g',3));
db1.clear();
db1.push_back(ui_->textEdit_d12->toPlainText().toDouble());
}
else{
ui_->textEdit_d13->append(QString::number(lin_value[1],'g',3));
db1.clear();
db1.push_back(ui_->textEdit_d13->toPlainText().toDouble());}

//set pmt2 gain value
ui_->textEdit_d21->append(ID[2]);
if(flag_GAIN==0){
ui_->textEdit_d22->append(QString::number(lin_value[2],'g',3));
db2.clear();
db2.push_back(ui_->textEdit_d22->toPlainText().toDouble());
}
else{
ui_->textEdit_d23->append(QString::number(lin_value[2],'g',3));
db2.clear();
db2.push_back(ui_->textEdit_d23->toPlainText().toDouble());}


//set pmt3 gain value
ui_->textEdit_d31->append(ID[3]);
if(flag_GAIN==0){
ui_->textEdit_d32->append(QString::number(lin_value[3],'g',3));
db3.clear();
db3.push_back(ui_->textEdit_d32->toPlainText().toDouble());
}
else{
ui_->textEdit_d33->append(QString::number(lin_value[3],'g',3));
db3.clear();
db3.push_back(ui_->textEdit_d33->toPlainText().toDouble());}

//set pmt4 gain value

ui_->textEdit_d41->append(ID[4]);
if(flag_GAIN==0){
ui_->textEdit_d42->append(QString::number(lin_value[4],'g',3));
db4.clear();
db4.push_back(ui_->textEdit_d42->toPlainText().toDouble());
}
else{
ui_->textEdit_d43->append(QString::number(lin_value[4],'g',3));
db4.clear();
db4.push_back(ui_->textEdit_d43->toPlainText().toDouble());}

//set pmt5 gain value
ui_->textEdit_d51->append(ID[5]);
if(flag_GAIN==0){
ui_->textEdit_d52->append(QString::number(lin_value[5],'g',3));
db5.clear();
db5.push_back(ui_->textEdit_d52->toPlainText().toDouble());
}
else{
ui_->textEdit_d53->append(QString::number(lin_value[5],'g',3));
db5.clear();
db5.push_back(ui_->textEdit_d53->toPlainText().toDouble());}

//set pmt6 gain value
ui_->textEdit_d61->append(ID[6]);
if(flag_GAIN==0){
ui_->textEdit_d62->append(QString::number(lin_value[6],'g',3));
db6.clear();
db6.push_back(ui_->textEdit_d62->toPlainText().toDouble());
}
else{
ui_->textEdit_d63->append(QString::number(lin_value[6],'g',3));
db6.clear();
db6.push_back(ui_->textEdit_d63->toPlainText().toDouble());}


//set pmt7 gain value
ui_->textEdit_d71->append(ID[7]);
if(flag_GAIN==0){
ui_->textEdit_d72->append(QString::number(lin_value[7],'g',3));
db7.clear();
db7.push_back(ui_->textEdit_d72->toPlainText().toDouble());
}
else{
ui_->textEdit_d73->append(QString::number(lin_value[7],'g',3));
db7.clear();
db7.push_back(ui_->textEdit_d73->toPlainText().toDouble());}


//set pmt8 gain value
ui_->textEdit_d81->append(ID[8]);
if(flag_GAIN==0){
ui_->textEdit_d82->append(QString::number(lin_value[8],'g',3));
db8.clear();
db8.push_back(ui_->textEdit_d82->toPlainText().toDouble());
}
else{
ui_->textEdit_d83->append(QString::number(lin_value[8],'g',3));
db8.clear();
db8.push_back(ui_->textEdit_d83->toPlainText().toDouble());}

//set pmt9 gain value
ui_->textEdit_d91->append(ID[9]);
if(flag_GAIN==0){
ui_->textEdit_d92->append(QString::number(lin_value[9],'g',3));
db9.clear();
db9.push_back(ui_->textEdit_d92->toPlainText().toDouble());
}
else{
ui_->textEdit_d93->append(QString::number(lin_value[9],'g',3));
db9.clear();
db9.push_back(ui_->textEdit_d93->toPlainText().toDouble());}

//set pmt10 gain value
ui_->textEdit_d101->append(ID[10]);
if(flag_GAIN==0){
ui_->textEdit_d102->append(QString::number(lin_value[10],'g',3));
db10.clear();
db10.push_back(ui_->textEdit_d102->toPlainText().toDouble());
}
else{
ui_->textEdit_d103->append(QString::number(lin_value[10],'g',3));
db10.clear();
db10.push_back(ui_->textEdit_d103->toPlainText().toDouble());}

//set pmt11 gain value
ui_->textEdit_d111->append(ID[11]);
if(flag_GAIN==0){
ui_->textEdit_d112->append(QString::number(lin_value[11],'g',3));
db11.clear();
db11.push_back(ui_->textEdit_d112->toPlainText().toDouble());
}
else{
ui_->textEdit_d113->append(QString::number(lin_value[11],'g',3));
db11.clear();
db11.push_back(ui_->textEdit_d113->toPlainText().toDouble());}

//set pmt12 gain value
ui_->textEdit_d121->append(ID[12]);
if(flag_GAIN==0){
ui_->textEdit_d122->append(QString::number(lin_value[12],'g',3));
db12.clear();
db12.push_back(ui_->textEdit_d122->toPlainText().toDouble());
}
else{
ui_->textEdit_d123->append(QString::number(lin_value[12],'g',3));
db12.clear();
db12.push_back(ui_->textEdit_d123->toPlainText().toDouble());}

//set pmt13 gain value
ui_->textEdit_d131->append(ID[13]);
if(flag_GAIN==0){
ui_->textEdit_d132->append(QString::number(lin_value[13],'g',3));
db13.clear();
db13.push_back(ui_->textEdit_d132->toPlainText().toDouble());
}
else{
ui_->textEdit_d133->append(QString::number(lin_value[13],'g',3));
db13.clear();
db13.push_back(ui_->textEdit_d133->toPlainText().toDouble());}

//set pmt14 gain value
ui_->textEdit_d141->append(ID[14]);
if(flag_GAIN==0){
ui_->textEdit_d142->append(QString::number(lin_value[14],'g',3));
db14.clear();
db14.push_back(ui_->textEdit_d142->toPlainText().toDouble());
}
else{
ui_->textEdit_d143->append(QString::number(lin_value[14],'g',3));
db14.clear();
db14.push_back(ui_->textEdit_d143->toPlainText().toDouble());}

//set pmt15 gain value
ui_->textEdit_d151->append(ID[15]);
if(flag_GAIN==0){
ui_->textEdit_d152->append(QString::number(lin_value[15],'g',3));
db15.clear();
db15.push_back(ui_->textEdit_d152->toPlainText().toDouble());
}
else{
ui_->textEdit_d153->append(QString::number(lin_value[15],'g',3));
db15.clear();
db15.push_back(ui_->textEdit_d153->toPlainText().toDouble());}

}



int fill_db::connect(){

QString user = ui_->lineEdit_user->text();
QString password = ui_->lineEdit_psw->text();

const char* configfile ="db_config.dat";


string tmp  ;
ifstream config(configfile);

getline(config,tmp); //1st line
getline(config,tmp); //2nd line
QString hostname= QString::fromStdString(tmp);
//qDebug()<<hostname;
getline(config,tmp); //3rd line
getline(config,tmp); //4th line
QString dbName= QString::fromStdString(tmp);


        db = QSqlDatabase::addDatabase("QMYSQL");
        db.setHostName(hostname);
        db.setDatabaseName(dbName);
        db.setUserName(user);
        db.setPassword(password);
        int ok = db.open();
    
   		if(ok) {cout<<"Connected to Database"<<endl; //return 1 if connected
			ui_->textEdit_dbStatus->clear();
			ui_->textEdit_dbStatus->append("Connected");
			return ok;
			}
	
		else	{cout<<"Connection failed!"<<endl;
			return 0;}

			}


void fill_db::disconnect(){

db.close();
ui_->textEdit_dbStatus->clear();
ui_->textEdit_dbStatus->append("Disconnected");
}

void fill_db::clear(){
    //set pmt0 gain value
    ui_->textEdit_d01->clear();
    ui_->textEdit_d02->clear();
    ui_->textEdit_d03->clear();
    db0.clear();


    //set pmt1 gain value
    ui_->textEdit_d11->clear();
    ui_->textEdit_d12->clear();
    ui_->textEdit_d13->clear();
    db1.clear();

    //set pmt2 gain value
    ui_->textEdit_d21->clear();
    ui_->textEdit_d22->clear();
    ui_->textEdit_d23->clear();
    db2.clear();


    //set pmt3 gain value
    ui_->textEdit_d31->clear();
    ui_->textEdit_d32->clear();
    ui_->textEdit_d33->clear();
    db3.clear();

    //set pmt4 gain value
    ui_->textEdit_d41->clear();
    ui_->textEdit_d42->clear();
    ui_->textEdit_d43->clear();
    db4.clear();


    //set pmt5 gain value
    ui_->textEdit_d51->clear();
    ui_->textEdit_d52->clear();
    ui_->textEdit_d53->clear();
    db5.clear();

    //set pmt6 gain value
    ui_->textEdit_d61->clear();
    ui_->textEdit_d62->clear();
    ui_->textEdit_d63->clear();
    db6.clear();


    //set pmt7 gain value
    ui_->textEdit_d71->clear();
    ui_->textEdit_d72->clear();
    ui_->textEdit_d73->clear();
    db7.clear();


    //set pmt8 gain value
    ui_->textEdit_d81->clear();
    ui_->textEdit_d82->clear();
    ui_->textEdit_d83->clear();
    db8.clear();

    //set pmt9 gain value
    ui_->textEdit_d91->clear();
    ui_->textEdit_d92->clear();
    ui_->textEdit_d93->clear();
    db9.clear();

    //set pmt10 gain value
    ui_->textEdit_d101->clear();
    ui_->textEdit_d102->clear();
    ui_->textEdit_d103->clear();
    db10.clear();

    //set pmt11 gain value
    ui_->textEdit_d111->clear();
    ui_->textEdit_d112->clear();
    ui_->textEdit_d113->clear();
    db11.clear();


    //set pmt12 gain value
    ui_->textEdit_d121->clear();
    ui_->textEdit_d122->clear();
    ui_->textEdit_d123->clear();
    db12.clear();

    //set pmt13 gain value
    ui_->textEdit_d131->clear();
    ui_->textEdit_d132->clear();
    ui_->textEdit_d133->clear();
    db13.clear();

    //set pmt14 gain value
    ui_->textEdit_d141->clear();
    ui_->textEdit_d142->clear();
    ui_->textEdit_d143->clear();
    db14.clear();

    //set pmt15 gain value
    ui_->textEdit_d151->clear();
    ui_->textEdit_d152->clear();
    ui_->textEdit_d153->clear();
    db15.clear();



    }

void fill_db::store_all_data(){

const char* configfile ="config.dat";
string tmp  ;
ifstream config(configfile);
for(unsigned int i=0;i<20;i++) getline(config,tmp); //read 12 lines
QString webpage= QString::fromStdString(tmp);
vector<QString> gain_web , linH_web , linL_web;

//QSqlQuery query("INSERT INTO pmt VALUES [ID , p0 , p1 , HV1 , HV2] ( db0[0], db0[1], db0[2],db0[3],db0[4]" );

vector<vector<double>> vec_db;
vec_db.push_back(db0);
vec_db.push_back(db1);
vec_db.push_back(db2);
vec_db.push_back(db3);
vec_db.push_back(db4);
vec_db.push_back(db5);
vec_db.push_back(db6);
vec_db.push_back(db7);
vec_db.push_back(db8);
vec_db.push_back(db9);
vec_db.push_back(db10);
vec_db.push_back(db11);
vec_db.push_back(db12);
vec_db.push_back(db13);
vec_db.push_back(db14);
vec_db.push_back(db15);

for(unsigned int i=0; i<vec_db.size();i++){
QString wb = webpage;

wb.append("/");
wb.append(IDpmt[i]);
wb.append("/");

QString wb1 = wb;
QString wb2 = wb;
QString wb3 = wb;


linH_web.push_back(wb2.append("linearity_High.pdf"));
linL_web.push_back(wb3.append("linearity_Low.pdf"));


}


QString queryString , queryString2;
QTextStream queryStream(&queryString);
QTextStream queryStream2(&queryString2);
//QSqlQuery query( "" );
if(flag_GAIN==0){
for(unsigned int i=0; i<vec_db.size();i++){
queryString.clear();
queryString2.clear();

queryStream<<"UPDATE pmt SET currentH = "<< vec_db[i][0] <<" WHERE id = '"<< IDpmt[i] << "' AND RUN = '" << RUN <<"';";
QSqlQuery query( *queryStream.string() );

queryStream2<<"UPDATE pmt SET pdf_linH = '"<< linH_web[i] <<"' WHERE id = '"<< IDpmt[i] << "' AND RUN = '" << RUN <<"';";
QSqlQuery query2( *queryStream2.string() );
//qDebug()<<*queryStream2.string();

}
}
else{
for(unsigned int i=0; i<vec_db.size();i++){
queryString.clear();
queryString2.clear();

queryStream<<"UPDATE pmt SET currentL = "<< vec_db[i][0] <<" WHERE id = '"<< IDpmt[i] << "' AND RUN = '" << RUN <<"';";
QSqlQuery query( *queryStream.string() );

queryStream2<<"UPDATE pmt SET pdf_linL = '"<< linL_web[i] <<"' WHERE id = '"<< IDpmt[i] << "' AND RUN = '" << RUN <<"';";
QSqlQuery query2( *queryStream2.string() );

}



} 
}
void fill_db::store_data(vector<int> flags){


const char* configfile ="config.dat";
string tmp  ;
ifstream config(configfile);
for(unsigned int i=0;i<20;i++) getline(config,tmp); //read 12 lines
QString webpage= QString::fromStdString(tmp);
vector<QString> gain_web , linH_web , linL_web;

for(unsigned int i=0; i<16;i++){
QString wb = webpage;

wb.append("/");
wb.append(IDpmt[i]);
wb.append("/");

QString wb1 = wb;
QString wb2 = wb;
QString wb3 = wb;


linH_web.push_back(wb2.append("linearity_High.pdf"));
linL_web.push_back(wb3.append("linearity_Low.pdf"));

}
//QSqlQuery query("INSERT INTO pmt VALUES [ID , p0 , p1 , HV1 , HV2] ( db0[0], db0[1], db0[2],db0[3],db0[4]" );

vector<vector<double>> vec_db;
vector<QString> selID;
vector<QString> linHW;
vector<QString> linLW;

//for(int i=0;i<flags.size();i++) cout<<flags[i]<<" "<<IDpmt[i].toStdString()<<endl;
if(flags[0]==1)  {vec_db.push_back(db0);  selID.push_back(IDpmt[0]  ); linHW.push_back(linH_web[0] );linLW.push_back(linL_web[0] );}
if(flags[1]==1)  {vec_db.push_back(db1);  selID.push_back(IDpmt[1]  ); linHW.push_back(linH_web[1] );linLW.push_back(linL_web[1] );}
if(flags[2]==1)  {vec_db.push_back(db2);  selID.push_back(IDpmt[2]  ); linHW.push_back(linH_web[2] );linLW.push_back(linL_web[2] );}
if(flags[3]==1)  {vec_db.push_back(db3);  selID.push_back(IDpmt[3]  ); linHW.push_back(linH_web[3] );linLW.push_back(linL_web[3] );}
if(flags[4]==1)  {vec_db.push_back(db4);  selID.push_back(IDpmt[4]  ); linHW.push_back(linH_web[4] );linLW.push_back(linL_web[4] );}
if(flags[5]==1)  {vec_db.push_back(db5);  selID.push_back(IDpmt[5]  ); linHW.push_back(linH_web[5] );linLW.push_back(linL_web[5] );}
if(flags[6]==1)  {vec_db.push_back(db6);  selID.push_back(IDpmt[6]  ); linHW.push_back(linH_web[6] );linLW.push_back(linL_web[6] );}
if(flags[7]==1)  {vec_db.push_back(db7);  selID.push_back(IDpmt[7]  ); linHW.push_back(linH_web[7] );linLW.push_back(linL_web[7] );}
if(flags[8]==1)  {vec_db.push_back(db8);  selID.push_back(IDpmt[8]  ); linHW.push_back(linH_web[8] );linLW.push_back(linL_web[8] );}
if(flags[9]==1)  {vec_db.push_back(db9);  selID.push_back(IDpmt[9]  ); linHW.push_back(linH_web[9] );linLW.push_back(linL_web[9] );}
if(flags[10]==1) {vec_db.push_back(db10); selID.push_back(IDpmt[10] ); linHW.push_back(linH_web[10] );linLW.push_back(linL_web[10] );}
if(flags[11]==1) {vec_db.push_back(db11); selID.push_back(IDpmt[11] ); linHW.push_back(linH_web[11] );linLW.push_back(linL_web[11] );}
if(flags[12]==1) {vec_db.push_back(db12); selID.push_back(IDpmt[12] ); linHW.push_back(linH_web[12] );linLW.push_back(linL_web[12] );}
if(flags[13]==1) {vec_db.push_back(db13); selID.push_back(IDpmt[13] ); linHW.push_back(linH_web[13] );linLW.push_back(linL_web[13] );}
if(flags[14]==1) {vec_db.push_back(db14); selID.push_back(IDpmt[14] ); linHW.push_back(linH_web[14] );linLW.push_back(linL_web[14] );}
if(flags[15]==1) {vec_db.push_back(db15); selID.push_back(IDpmt[15] ); linHW.push_back(linH_web[15] );linLW.push_back(linL_web[15] );}

//cout<<vec_db.size()<<endl;
//cout<<selID.size()<<endl;

QString queryString , queryString2;
QTextStream queryStream(&queryString);
QTextStream queryStream2(&queryString2);
//QSqlQuery query( "" );
if(flag_GAIN==0){
for(unsigned int i=0; i<vec_db.size();i++){
queryString.clear();
queryString2.clear();

queryStream<<"UPDATE pmt SET currentH = "<< vec_db[i][0] <<" WHERE id = '"<< selID[i] << "' AND RUN = '" << RUN <<"';";
//qDebug()<<*queryStream.string();
QSqlQuery query( *queryStream.string() );

queryStream2<<"UPDATE pmt SET pdf_linH = '"<< linHW[i] <<"' WHERE id = '"<< selID[i] << "' AND RUN = '" << RUN <<"';";
QSqlQuery query2( *queryStream2.string() );

}
}
else{
for(unsigned int i=0; i<vec_db.size();i++){
queryString.clear();
queryString2.clear();
queryStream<<"UPDATE pmt SET currentL = "<< vec_db[i][0] <<" WHERE id = '"<< selID[i] << "' AND RUN = '" << RUN <<"';";

queryStream2<<"UPDATE pmt SET pdf_linL = '"<< linLW[i] <<"' WHERE id = '"<< selID[i] << "' AND RUN = '" << RUN <<"';";
QSqlQuery query2( *queryStream2.string() );



//qDebug()<<*queryStream.string();
QSqlQuery query( *queryStream.string() );
}

}


}

void fill_db::load_data(){
QString index = ui_->lineEdit_ID->text();

ui_->textEdit_p0->clear();
ui_->textEdit_p1->clear();
ui_->textEdit_HV1->clear();
ui_->textEdit_HV2->clear();
ui_->textEdit_peak->clear();
ui_->textEdit_peak_2->clear();
ui_->textEdit_DCR->clear();
ui_->textEdit_RUN->clear();


QString queryString;
QTextStream queryStream(&queryString);

queryStream<<"SELECT p0 FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query1(*queryStream.string());
query1.next();
//QString p0 = query1.value(0).toString();
QString p0 = QString::number(query1.value(0).toDouble(),'e',2);
ui_->textEdit_p0->append(p0);
queryString.clear();

queryStream<<"SELECT p1 FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query2(*queryStream.string());
query2.next();
QString p1 = QString::number(query2.value(0).toDouble(),'g',2);
ui_->textEdit_p1->append(p1);
queryString.clear();


queryStream<<"SELECT HV1 FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query3(*queryStream.string());
query3.next();
QString HV1 = query3.value(0).toString();
ui_->textEdit_HV1->append(HV1);
queryString.clear();


queryStream<<"SELECT HV2 FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query4(*queryStream.string());
query4.next();
QString HV2 = query4.value(0).toString();
ui_->textEdit_HV2->append(HV2);
queryString.clear();

queryStream<<"SELECT currentH FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query5(*queryStream.string());
query5.next();
//QString peak = query5.value(0).toString();
QString peak = QString::number(query5.value(0).toDouble(),'g',2);

//qDebug()<<peak;
ui_->textEdit_peak->append(peak);
queryString.clear();


queryStream<<"SELECT DCR FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query6(*queryStream.string());
query6.next();
QString dcr = query6.value(0).toString();
ui_->textEdit_DCR->append(dcr);
queryString.clear();


queryStream<<"SELECT RUN FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query7(*queryStream.string());
query7.next();
QString RUN = query7.value(0).toString();
ui_->textEdit_RUN->append(RUN);
queryString.clear();


queryStream<<"SELECT currentL FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query8(*queryStream.string());
query8.next();
QString peak2 = QString::number(query8.value(0).toDouble(),'g',2);
ui_->textEdit_peak_2->append(peak2);
queryString.clear();

}





/*///////////////////////COMANDI MYSQL
CREATE DATABASE Augerpmt;

CREATE TABLE pmt (RUN varchar(10),ID varchar(10), p0 double, p1 double , HV1 double , HV2 double , DCR double, currentH double,  currentL double);

show databases;
use pmt;
show tables;
select * from pmt;
select ID from pmt;

INSERT INTO pmt (RUN, ID , p0 , p1 , HV1 , HV2) VALUES ('000','aaa' , 1.5 , 2e2 , 1450.4  , 129);

UPDATE pmt SET current = 155 WHERE id = 'aaa';

DELETE FROM pmt WHERE ID='1' AND RUN='100';
*/




