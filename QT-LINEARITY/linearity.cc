#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <iomanip>  
#include <QProcess>

#include <TMath.h>
#include <TFile.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TFrame.h>
#include <TGraphErrors.h>
#include <TLegend.h>
#include <TLine.h>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include "linearity.h"

using namespace std;



/*///////
LEGGE FILE DOVE PRIMA CI SONO I DATI HIGH SIGNAL E POI LOW SIGNAL
/*////////////////////////////////////////////////




////////////////
////////////////////////////////////////////////
linearity::linearity (  QString ID, int flag_lowgain)
{

int  n_ev ;
double attenuation,  thresholdHigh,  thresholdLow;

/////////FILE LINEARITY


const char* configfile ="config.dat";

QString tmp, path,pdf_path,path1, att, event_n, threshHigh1, threshLow1  ;
QString path2, threshHigh2, threshLow2 ;
QFile config(configfile);
QTextStream in(&config);
if(!config.open(QIODevice::ReadOnly | QIODevice::Text)) cout<<"config file not found!"<<endl;
tmp = in.readLine();
path1 = in.readLine();
tmp = in.readLine();
att = in.readLine();
tmp = in.readLine();
event_n = in.readLine();
tmp = in.readLine();
threshHigh1 = in.readLine();
tmp = in.readLine();
threshLow1 = in.readLine();
tmp = in.readLine();
path2 = in.readLine();
tmp = in.readLine();
threshHigh2 = in.readLine();
tmp = in.readLine();
threshLow2 = in.readLine();
tmp = in.readLine();
pdf_path=in.readLine();

attenuation=att.toDouble();
n_ev=event_n.toDouble();

if (flag_lowgain == 0){
thresholdHigh=threshHigh1.toDouble();
thresholdLow=threshLow1.toDouble();
path=path1;
}
else{thresholdHigh=threshHigh2.toDouble();
thresholdLow=threshLow2.toDouble();
path=path2;
}
//QString path ="linearity/";
path.append("/");
path.append(ID);

pdf_path.append("/");
pdf_path.append(ID);
QString pdf_filename;
if (flag_lowgain == 0)  pdf_filename = pdf_path.append("/linearity_High.pdf");
else  pdf_filename = pdf_path.append("/linearity_Low.pdf");

string file_lin= path.toStdString();

file_lin.append("/lin_output.txt");

ifstream path_lin(file_lin.c_str());

//cout<<thresholdLow<<" "<< thresholdHigh<<endl;
//cout<<path.toStdString()<<endl;

string line;



getline(path_lin,line);
//cout<<" path: "<<line<<endl;



vector<double> peak_low, peak_high, charge_low, charge_high, err_peak_low, err_peak_high, err_charge_low, err_charge_high, n_events, vec_ratio, vec_ratio_err;
 


double peakH, err_peakH, chargeH, err_chargeH, peakL, err_peakL, chargeL, err_chargeL, av_ratio, av_ratio_err ;
int counts;




 if (path_lin.is_open()){		


	while(path_lin.good() )

				{

		path_lin>>peakH>>err_peakH>>chargeH>>err_chargeH>>
			peakL>>err_peakL>>chargeL>>err_chargeL>>av_ratio>>av_ratio_err;

		if(path_lin.eof()) break;
		peak_high.push_back(peakH);
		err_peak_high.push_back(err_peakH);
		charge_high.push_back(chargeH); 
		err_charge_high.push_back(err_chargeH);
	
		peak_low.push_back(peakL);
		err_peak_low.push_back(err_peakL); 
		charge_low.push_back(chargeL);
		err_charge_low.push_back(err_chargeL);

		n_events.push_back(n_ev);

//vec_ratio.push_back(chargeH/chargeL);
vec_ratio.push_back(av_ratio);
vec_ratio_err.push_back(av_ratio_err/sqrt(n_ev));
				}
		}

counts = peak_high.size();
vector<double> peak_current, peak , ratio , ratio_errors , peak_err ;
vector<double> NL, err_diff, err_NL ;

/*
//cout<<counts<<endl;
for(int i=0 ; i<counts ; i++){ 

peak_current.push_back(peak_high[i]/50*attenuation);  //mA

peak_err.push_back(err_peak_high[i]/50/n_events[i]*attenuation);  //mA

ratio.push_back(charge_high[i]/charge_low[i]);

ratio_errors.push_back(ratio[i]*sqrt(  pow(err_charge_low[i]/charge_low[i],2)/n_events[i]+pow(err_charge_high[i]/charge_high[i],2)/n_events[i]));  //errore su chargeH/chargeL

//cout<<ratio_errors.at(i)<<endl;
}

int j=0;
double norm=1.;
//norm= 10.34;//ratio[1];
for(int i=0;i<counts;i++)
		 if(peak_current[i]<threshold) {norm=norm+ratio[i]; j++; }
norm=norm/j;
//cout<<norm<<endl;



double err_norm=0;
for (int i=0;i<counts;i++) { 


///carica
NL.push_back(100*(ratio[i] -norm)/norm);

err_diff.push_back(sqrt(ratio_errors[i]*ratio_errors[i]+err_norm*err_norm));

err_NL.push_back(fabs(NL[i]*sqrt(  pow(err_diff[i]/(ratio[i] -norm),2))));


}
*/

for(int i=0 ; i<counts ; i++) {peak_current.push_back(peak_high[i]/50*attenuation);  //mA
      			       peak_err.push_back(err_peak_high[i]/50/sqrt(n_events[i])*attenuation); } //mA

double err_norm=0;
int j=0;
double norm=0.; 
for(int i=0;i<counts;i++)
		 if(peak_current[i]<thresholdHigh&&peak_current[i]>thresholdLow) {norm=norm+vec_ratio[i]; j++; }
norm=norm/j;
cout<<"norm = "<<norm<<endl;

for(int i=0 ; i<counts ; i++) {
NL.push_back(100*(vec_ratio[i] -norm)/norm);
err_diff.push_back(sqrt(vec_ratio_err[i]*vec_ratio_err[i]+err_norm*err_norm));
err_NL.push_back(fabs(NL[i]*sqrt(  pow(err_diff[i]/(vec_ratio[i] -norm),2))));
}


//////////calcolo NL 5%
double limit=-5.; //soglia al 5%
double tmp_low=-15., tmp_high=0.;
int curr_min=-1 , curr_max=-1;
for(unsigned int i=0; i<NL.size();i++){
if(peak_current[i]>thresholdLow){
if(NL[i]<limit&&NL[i]>tmp_low) {tmp_low=NL[i]; curr_min=i;}
if(NL[i]>limit&&NL[i]<tmp_high) {tmp_high=NL[i]; curr_max=i;}
}
}
//interpolation tmp_low tmp_high

nl5 = (limit - tmp_low)/(tmp_high-tmp_low)*(peak_current[curr_max]-peak_current[curr_min])+ peak_current[curr_min];
cout<<"non lin 5% = "<<nl5<<endl;
//for(unsigned int i=0; i<NL.size();i++){
//cout<<NL[i]<<" low = "<<tmp_low<<" high = "<<tmp_high<<" i min = "<<curr_min<<" i max =  "<<curr_max<<" current = "<<peak_current[i]<<endl;}



//////////////////////


QString root_filename = path.append("/linearity.root");
TFile *f1 = new TFile(root_filename.toStdString().c_str(),"recreate");

TGraphErrors* lin_plot = new TGraphErrors (counts,&peak_current[0], &NL[0],&peak_err[0],&err_NL[0]);




lin_plot->GetXaxis()->SetTitle("Peak current [mA]");
lin_plot->GetYaxis()->SetTitle("NL %");
//lin_plot->GetXaxis()->SetRangeUser(0.,250.);
lin_plot->GetXaxis()->SetLimits(0, 500);
lin_plot->GetYaxis()->SetRangeUser(-10,6);

lin_plot->Draw("AP");


QString title ="PMT ID ";
title.append(ID);

lin_plot->SetTitle(title.toStdString().c_str());


lin_plot->SetName("lin");

lin_plot->SetMarkerStyle(20);
lin_plot->SetMarkerSize(0.7);
lin_plot->SetMarkerColor(4);
lin_plot->SetLineColor(4);
lin_plot->SetLineWidth(2);




lin_plot->Draw("Ap");


lin_plot->Write();

f1->Close();


QProcess *process = new QProcess();
QString proc;
 proc="./print_lin.sh ";

proc.append(root_filename);
proc.append(" ");
proc.append(pdf_filename);
if(flag_lowgain == 0) proc.append(" 1 ");
else proc.append(" 2 ");
//cout<<proc.toStdString()<<endl;
//process->start(proc);

}
