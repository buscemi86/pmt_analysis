#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDesktopServices>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->analyzeButton->setEnabled(false);
    ui->analyze_histo->setEnabled(false);
    ui->clearButton->setEnabled(false);
    ui->storeAll_Button->setEnabled(false);
    ui->storeButton->setEnabled(false);
    ui->drawAllbutton->setEnabled(false);
    
    ui->lineEdit_psw->setEchoMode(QLineEdit::Password);
    ui->textEdit_dbStatus->clear();
    ui->textEdit_dbStatus->append("Disconnected");

config = new configuration(ui);
list = new mk_list();
db = new fill_db(ui);

histogram = new single_histo();
flag_spe=0;
status_list=1;
status_an=1;
status_histo=1;
flag_lowgain=0;

TFile* out = new TFile("out.root","recreate");


}

MainWindow::~MainWindow()
{
    delete ui;
}


/*
void MainWindow::on_checkBox_stateChanged(int arg1)
{
flag_lowgain=arg1;
cout<<"flaglowgain = "<<flag_lowgain<<endl;
};
*/

void MainWindow::on_chooseButton_clicked()
{
flag_lowgain=MainWindow::loadDataFiles(); //to choose directory of data-files to be analyzed 
status_list =list->setDir(Directory);
if (status_list==0){
ui->label_28->setEnabled(true);
ui->analyzeButton->setEnabled(true);
 ui->textEdit_21->clear();
//ui->textEdit_21->setEnabled(true);
status_list=1;		}

}

void MainWindow::on_analyzeButton_clicked()
{
pmt_data = new write_pmt();
db->clear();

ui->textEdit_21->clear();
lin_value.clear();
 ui->textEdit_21->append("In Progress");
MainWindow::delay();
ID=list->getID();
pmt_data->analyze(list->get_list());

status_an = pmt_data->save_file(ID, flag_lowgain);

list->remove();
if(status_an==0)
            {ui->textEdit_21->clear();
             ui->textEdit_21->append( "Completed");
	     ui->storeAll_Button->setEnabled(true);
	     ui->storeButton->setEnabled(true);
             status_an =0;}

else {ui->textEdit_21->clear();
ui->textEdit_21->append( "ERROR");
status_an=1;
}

cout<<"STATUS= "<<status_an<<endl;
	if(status_an==0)	{
			for(unsigned int i=0; i<ID.size();i++)	{
				linearity* plot_lin = new linearity( ID[i] , flag_lowgain);
				lin_value.push_back(plot_lin->get_value());		}	
			}


db->show_data(ID,lin_value, flag_lowgain, RunNumber);
ui->clearButton->setEnabled(true);
//status_an=1;
						
delete pmt_data;
ui->drawAllbutton->setEnabled(true);
ui->clearButton->setEnabled(false);						
cout<<"LINEARITY ANALYSIS COMPLETED"<<endl;
}


///TO PRINT LINEARITY PLOT
void MainWindow::on_drawButton_clicked()
{
 QString   IDdraw = QFileDialog::getExistingDirectory(this,
                   tr("Choose PMT Index"),
                   "./",
                  QFileDialog::DontResolveSymlinks);


ui->textEdit_22->clear();

QFileInfo file(IDdraw);
if (!IDdraw.isEmpty()){ ui->textEdit_22->append(file.fileName());


QString root_filename=IDdraw;
root_filename.append("/linearity.root");

QProcess *process = new QProcess();
QString proc="./lin_plot.sh ";
proc.append(root_filename);
//proc.append(" ");
//proc.append(IDdraw);
cout<<proc.toStdString()<<endl;
process->start(proc);
//ui->textEdit_22->clear();
cout<<"END"<<endl;



}
}


void MainWindow::on_drawAllbutton_clicked()
{

const char* configfile ="config.dat";

QFile config(configfile);
QTextStream in(&config);
QString tmp, config_path , path1 , path2;
if(!config.open(QIODevice::ReadOnly | QIODevice::Text)) cout<<"config file not found!"<<endl;

tmp = in.readLine();
path1 = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
tmp = in.readLine();
path2 = in.readLine();




if (flag_lowgain == 0){

config_path=path1;
}
else{
config_path=path2;
}



QString root_filename = config_path;


//root_filename.append("/gain.root");
QProcess *process = new QProcess();
QString proc;
if(flag_lowgain==0)proc="./lin_plot_all.sh 1 ";
else proc="./lin_plot_all.sh 2 ";

proc.append(root_filename);
proc.append(" ");

for(unsigned int i=0; i<ID.size();i++) {proc.append(ID[i]);proc.append("/linearity.root ");}
//cout<<endl;
//cout<<proc.toStdString()<<endl;
process->start(proc);

}










void MainWindow::on_choose_histo_clicked()
{
    fileName = QFileDialog::getOpenFileName(this,
        tr("Open Data File"), "../Data", tr("Data Files (*)"));
ui->textEdit_30->clear();
ui->textEdit_31->clear();
QFileInfo file(fileName);
ui->textEdit_30->append( file.fileName());
ui->analyze_histo->setEnabled(true);
ui->label_43->setEnabled(true);

status_histo=1;
}

void MainWindow::on_analyze_histo_clicked()
{
ui->textEdit_31->clear();
ui->textEdit_31->append("In Progress");
MainWindow::delay();
cout<<fileName.toStdString()<<endl;
string name =fileName.toStdString();

         status_histo = histogram->histo(name,1);


if(status_histo==0){    ui->textEdit_31->clear();
                  ui->textEdit_31->append( "Completed");
                  }
   
    else 	{     ui->textEdit_31->clear();
                  ui->textEdit_31->append( "ERROR");}

    QProcess* process = new QProcess();
    QString proc="root -l draw.C";
process->start(proc);
}








void MainWindow::on_checkBox_db0_stateChanged(int arg1)
{
if(arg1!=0) flag_db0=1;
else flag_db0=0;

}

void MainWindow::on_checkBox_db1_stateChanged(int arg1)
{
    if(arg1!=0) flag_db1=1;
    else flag_db1=0;
}

void MainWindow::on_checkBox_db2_stateChanged(int arg1)
{
    if(arg1!=0) flag_db2=1;
    else flag_db2=0;
}

void MainWindow::on_checkBox_db3_stateChanged(int arg1)
{
    if(arg1!=0) flag_db3=1;
    else flag_db3=0;
}

void MainWindow::on_checkBox_db4_stateChanged(int arg1)
{
    if(arg1!=0) flag_db4=1;
    else flag_db4=0;
}

void MainWindow::on_checkBox_db5_stateChanged(int arg1)
{
    if(arg1!=0) flag_db5=1;
    else flag_db5=0;
}

void MainWindow::on_checkBox_db6_stateChanged(int arg1)
{
    if(arg1!=0) flag_db6=1;
    else flag_db6=0;
}

void MainWindow::on_checkBox_db7_stateChanged(int arg1)
{
    if(arg1!=0) flag_db7=1;
    else flag_db7=0;
}

void MainWindow::on_checkBox_db8_stateChanged(int arg1)
{
    if(arg1!=0) flag_db8=1;
    else flag_db8=0;
}

void MainWindow::on_checkBox_db9_stateChanged(int arg1)
{
    if(arg1!=0) flag_db9=1;
    else flag_db9=0;
}

void MainWindow::on_checkBox_db10_stateChanged(int arg1)
{
    if(arg1!=0) flag_db10=1;
    else flag_db10=0;
}

void MainWindow::on_checkBox_db11_stateChanged(int arg1)
{
    if(arg1!=0) flag_db11=1;
    else flag_db11=0;
}

void MainWindow::on_checkBox_db12_stateChanged(int arg1)
{
    if(arg1!=0) flag_db12=1;
    else flag_db12=0;
}

void MainWindow::on_checkBox_db13_stateChanged(int arg1)
{
    if(arg1!=0) flag_db13=1;
    else flag_db13=0;
}

void MainWindow::on_checkBox_db14_stateChanged(int arg1)
{
    if(arg1!=0) flag_db14=1;
    else flag_db14=0;
}

void MainWindow::on_checkBox_db15_stateChanged(int arg1)
{
    if(arg1!=0) flag_db15=1;
    else flag_db15=0;
}






void MainWindow::on_clearButton_clicked()
{
    db->clear();

}

void MainWindow::on_connectButton_clicked()
{
flag_db =    db->connect();  //1=connected

    if(flag_db==1) {  
			qDebug()<<"DB Connected";		   
			}
else qDebug()<<"Connection Failed";
}


void MainWindow::on_storeAll_Button_clicked()
{if(status_an==0 && flag_db==1) {     db->store_all_data();
					qDebug()<<"All PMTs stored";}
else qDebug()<<"Connection Failed";
}


void MainWindow::on_uploadButton_clicked()
{
    db->load_data();
qDebug()<<"DB data loaded";
}

void MainWindow::on_disconnectButton_clicked()
{
   db->disconnect();
qDebug()<<"DB Disconnected";
}

void MainWindow::on_storeButton_clicked()
{
vector<int> vecDB;
vecDB.push_back(flag_db0);
vecDB.push_back(flag_db1);
vecDB.push_back(flag_db2);
vecDB.push_back(flag_db3);
vecDB.push_back(flag_db4);
vecDB.push_back(flag_db5);
vecDB.push_back(flag_db6);
vecDB.push_back(flag_db7);
vecDB.push_back(flag_db8);
vecDB.push_back(flag_db9);
vecDB.push_back(flag_db10);
vecDB.push_back(flag_db11);
vecDB.push_back(flag_db12);
vecDB.push_back(flag_db13);
vecDB.push_back(flag_db14);
vecDB.push_back(flag_db15);

//for(int i=0;i<vecDB.size();i++)cout<<vecDB[i]<<endl;

if(status_an==0 && flag_db==1) {db->store_data(vecDB);
				qDebug()<<"Selected PMTs stored";}

else qDebug()<<"Connection Failed";
}
