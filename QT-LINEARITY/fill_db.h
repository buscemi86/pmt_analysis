#ifndef DATABASE_H
#define DATABASE_H

#include <iostream>
#include <QMainWindow>
#include <QSqlDatabase>
#include <vector>
#include "ui_mainwindow.h"
using namespace std;

namespace Ui {
class MainWindow;
}

class fill_db{
public:
fill_db(Ui::MainWindow *ui);

void show_data(vector<QString> , vector<double> , int , QString);

void store_all_data();

void store_data(vector<int>);

void load_data();

void clear();

int connect();

void disconnect();


private slots:
void on_pushButton_clicked();




private:
Ui::MainWindow *ui_;
QSqlDatabase db;
int flag_GAIN;
QString RUN;
vector<QString> IDpmt;
vector<double> db0  , db1  , db2  , db3  , db4 ;
vector<double> db5  , db6  , db7  , db8  , db9 ;
vector<double> db10 , db11 , db12 , db13 , db14 , db15; 
};

#endif
