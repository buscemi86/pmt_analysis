#ifndef SINGLE_HISTO_H
#define SINGLE_HISTO_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <iomanip>  

#include <TMath.h>
#include <TTree.h>
#include <TFile.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TFrame.h>
#include <TF1.h>
#include <TH1F.h>
#include <TFitResult.h>

using namespace std;

class single_histo{

public:
int histo(string file_data, int f);

double get_mean_A1(){return mean_A1; }
double get_sigma_A1(){return sigma_A1; }
double get_mean_Q1(){return mean_Q1; }
double get_sigma_Q1(){return sigma_Q1; }
double get_mean_A2(){return mean_A2; }
double get_sigma_A2(){return sigma_A2; }
double get_mean_Q2(){return mean_Q2; }
double get_sigma_Q2(){return sigma_Q2; }
double get_mean_r(){return mean_r; }
double get_sigma_r(){return sigma_r; }

private:
//const char* filename;
int flag_fit;
//vector<double> vec_ch , vec_ampl;

double mean_A1, sigma_A1, mean_Q1, sigma_Q1;
double mean_A2, sigma_A2, mean_Q2, sigma_Q2;
double mean_r, sigma_r;

};

#endif
