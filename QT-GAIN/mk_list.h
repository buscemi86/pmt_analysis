#ifndef MK_LIST_H
#define MK_LIST_H

#include <QDebug>
#include <QDir>
#include <QStringList>
#include <iostream>
#include <QDirIterator>
#include <fstream>
#include <QString>
#include <QProcess>
#include <vector>
using namespace std;
class mk_list
{
public:

int setDir(QString Directory);
void remove();

vector<vector<QFileInfo>> get_list(){return vec_files;}
vector<QString> getID(){return vec_ID;}
vector<vector<double>> getHV(){return vec_HV;}
private:
QString pmt_ch0, pmt_ch1 ,pmt_ch2,pmt_ch3, pmt_ch4,pmt_ch5, pmt_ch6,pmt_ch7, pmt_ch8,pmt_ch9, pmt_ch10,pmt_ch11 ,pmt_ch12,pmt_ch13, pmt_ch14,pmt_ch15;
vector<double> HV_ch0, HV_ch1 ,HV_ch2,HV_ch3, HV_ch4,HV_ch5, HV_ch6,HV_ch7, HV_ch8,HV_ch9, HV_ch10,HV_ch11 ,HV_ch12,HV_ch13, HV_ch14,HV_ch15;

vector<QString> vec_ID ;
vector<vector<double>> vec_HV;

vector<QFileInfo> file_channel00, file_channel01, file_channel02, file_channel03, file_channel04, file_channel05, file_channel06, file_channel07, file_channel08, file_channel09, file_channel10, file_channel11, file_channel12, file_channel13, file_channel14, file_channel15;



vector<vector<QFileInfo>> vec_files;
};
#endif
