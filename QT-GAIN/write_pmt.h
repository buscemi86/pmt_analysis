#ifndef WRITE_PMT_H
#define WRITE_PMT_H

#include "read_file.h" 
#include <sstream>
#include <QString>
#include <QDir>
#include <QTextStream>
#include "RooPlot.h"
using namespace std;
class write_pmt{
public:
write_pmt(){};
void analyze(vector<vector<QFileInfo>> vec);
int save_file(vector<QString> ID);
vector<double> getSPEampl() {return spe_ampl;}

private:
int flag;
vector<double> pmt1, pmt2, pmt3, pmt4, pmt5, pmt6,pmt7,pmt8;
vector<double> pmt9, pmt10, pmt11, pmt12, pmt13, pmt14,pmt15,pmt16;
vector<vector<QFileInfo>> list;
vector<vector<vector<double>>> list_all_data;
vector<vector<TH1D*>> list_all_histo;
vector<RooPlot*> list_frame;
vector<double> spe_ampl;
};
#endif
