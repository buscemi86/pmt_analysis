#ifndef DARK_H
#define DARK_H

#include <QDebug>
#include <QDir>
#include <QStringList>
#include <iostream>
#include <QDirIterator>
#include <fstream>
#include <QString>
#include <QProcess>
#include <vector>
using namespace std;
class dark
{
public:

int setDir(QString Directory);
void remove();
void setSPE(vector<double> v){SPE_values=v;}
void clear_SPE(){SPE_values.clear();}
vector<QString> getID(){return vec_ID;}
int readDCR();
vector<double> getDCR(){return vec_DCR;}
private:
QString pmt_ch0, pmt_ch1 ,pmt_ch2,pmt_ch3, pmt_ch4,pmt_ch5, pmt_ch6,pmt_ch7, pmt_ch8,pmt_ch9, pmt_ch10,pmt_ch11 ,pmt_ch12,pmt_ch13, pmt_ch14,pmt_ch15;
vector<QString> vec_ID;
vector<QFileInfo> file_channel00, file_channel01, file_channel02, file_channel03, file_channel04, file_channel05, file_channel06, file_channel07, file_channel08, file_channel09, file_channel10, file_channel11, file_channel12, file_channel13, file_channel14, file_channel15;
vector<QFileInfo> vec_files;
vector<double> vec_DCR;
vector<double> SPE_values;
double BIN, SAMPLING,SHOTS;
};
#endif
