#include "single_histo.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooFFTConvPdf.h"
#include "RooPlot.h"
#include "RooDataHist.h"
#include "RooAddPdf.h"
#include "RooChebychev.h"
#include "RooExponential.h"

using namespace RooFit ;

int single_histo::histo(string file_data, int f){

cout<<"#######INFO: SINGLE_HISTO"<<endl;
flag_fit=f;

double a , b, c, d;
TFile* out = new TFile("out.root","recreate");
 
ifstream file (file_data.c_str());

TF1 * g1_q = new TF1("m1","gaus",-300,200);
TF1 *   g2_q = new TF1("m2","gaus",300,1800);
TF1*   fgaus2_q = new TF1("fgaus2","gaus(0)+gaus(3)+expo(6)",-300,5000);

TF1*   fgaus_q = new TF1("fgaus_a","gaus",-300,2000); 
TF1*   fgaus_a = new TF1("fgaus_q","gaus",0,1);

TF1 * g1_a = new TF1("n1","gaus",-0.01,0.03);
TF1 *   g2_a = new TF1("n2","gaus",0.05,1);
TF1*   fgaus2_a = new TF1("fgaus2_a","gaus(0)+gaus(3)+expo(6)",-0.01,1);

// Declare observable x
   RooRealVar x("Charge Integral [pVs]","Charge Integral [pVs]",-300,3000) ;
   RooRealVar y("y","Peak Amplitude [V]",0,1) ;
RooPlot* xframe  = x.frame(Title("SPE Charge Spectrum")) ;
 RooPlot* xframe2 = (RooPlot*) xframe->Clone("xframe2") ;
RooPlot* xframe3 = (RooPlot*) xframe->Clone("xframe") ;

RooPlot* xframeA  = y.frame(Title("SPE Amplitude Spectrum")) ;
 RooPlot* xframe2A = (RooPlot*) xframeA->Clone("xframe2A") ;

Double_t par[6];
 


TH1D* t1=new TH1D("t1","Ampl1",1024,0,1);
TH1D* t2=new TH1D("t2","Charge1",1500,-1000,20000);
t1->GetXaxis()->SetTitle("Peak Ampl [V]");
t1->GetYaxis()->SetTitle("counts");

t2->GetXaxis()->SetTitle("Charge Integral [pVs]");
t2->GetYaxis()->SetTitle("counts");





while(file>>a>>b>>c>>d)
{

t1->Fill(a);
t2->Fill(b);

}
file.close();
t1->Rebin(4);
t2->Rebin(4);
if(flag_fit==1){



t1->Fit("fgaus_a");
TF1 *fit1 = t1->GetFunction("fgaus_a");
mean_A = fit1->GetParameter(1);
sigma_A = fit1->GetParameter(2);


t2->Fit("fgaus_q");
TF1 *fit2 = t2->GetFunction("fgaus_q");
mean_Q = fit2->GetParameter(1);
sigma_Q = fit2->GetParameter(2);

cout<<"Ampl = "<<mean_A*1000<<" mV\t"<<"sigma Ampl = "<<sigma_A*1000<<" mV\t"<<"Charge = "<<mean_Q<<" pVs\t"<<"sigma Charge = "<<sigma_Q<<" pVs"<<endl;
}
  






else if(flag_fit==2){ //// SPE
t1->Fit("gaus","","",0.03,1);

//t2->Fit("gaus");
	TF1 * g1_q = new TF1("m1","gaus",-300,200);
	TF1 *   g2_q = new TF1("m2","gaus",300,1800);
	t2->Fit(g1_q,"R0");
	t2->Fit(g2_q,"R+0");
	// Get the parameters from the fit
	   g1_q->GetParameters(&par[0]);
	   g2_q->GetParameters(&par[3]);
	   // Use the parameters on the sum
	   fgaus2_q->SetParameters(par);
	t2->Fit(fgaus2_q,"R+");



 // Create 3 Gaussian PDFs sig0(x,mean1,sigma0), sig1(x,mean1,sigma1) and sig2(x,mean2,sigma2) and their parameters
   RooRealVar meanA0("mean","mean of gaussians",0,0,0.01) ;
   RooRealVar sigmaA0("sigma","width of gaussians",0.01,0,0.05) ;
   RooRealVar meanA1("mean1","meanA1",0.01,0.1,0.2) ;
   RooRealVar sigmaA1("sigma1","sigmaA1",0.01,0.005,0.2) ;

   RooRealVar meanA2("meanA2","meanA2",0.2,0.2,0.3) ;
   RooRealVar sigmaA2("sigmaA2","sigmaA2",0.1,0.01,0.3) ;

   RooGaussian sigA2("sigA2","Signal component 2",y,meanA2,sigmaA2) ;
   RooGaussian sigA0("sigA0","Signal component 0",y,meanA0,sigmaA0) ;
   RooGaussian sigA1("sigA1","Signal component 1",y,meanA1,sigmaA1) ;

   // Sum the signal components into a composite signal p.d.f.
   RooRealVar sig1Afrac("sig1Afrac","fraction of component 1 in signal",0.9,0.1,0.9) ;
   RooAddPdf sigA01("sigA01","SignalA",RooArgList(sigA0,sigA1),sig1Afrac) ;
   
   // Sum the signal components into a composite signal p.d.f.
   RooRealVar sig2Afrac("sig2frac","fraction of component 2 in signal",0.1,0.,1) ;
   RooAddPdf sigA("sigA","SignalA",RooArgList(sigA1,sigA2),sig2Afrac) ;

   // Build exponential pdf
   RooRealVar alphaA("alphaA","alphaA",-0.01) ;
   RooExponential bkg2A("bkg2","Background 2",y,alphaA) ;

   // Sum the background components into a composite background p.d.f.
 //  RooRealVar bkg1frac("sig1frac","fraction of component 1 in background",0.,0.,1.) ;
   RooAddPdf bkgA("bkgA","Signal",RooArgList(sigA0,bkg2A),sig1Afrac) ;

   // Sum the composite signal and background
   RooRealVar bkgfracA("bkgfracA","fraction of background",0.9,0.5,0.9) ;
   RooAddPdf  modelA("modelA","g1+g2+a",RooArgList(bkgA,sigA),bkgfracA) ;

RooDataHist dhA("dhA","dhA",y,t1) ;
sigA01.fitTo(dhA) ;
   // S e t u p   b a s i c   p l o t   w i t h   d a t a   a n d   f u l l   p d f
   // ------------------------------------------------------------------------------

   // Generate a data sample of 1000 events in x from model
   //RooDataSet *data = model.generate(x,1000) ;

   // Plot data and complete PDF overlaid
   
   dhA.plotOn(xframeA) ;


   // M a k e   c o m p o n e n t   b y   o b j e c t   r e f e r e n c e
   // --------------------------------------------------------------------

   modelA.plotOn(xframeA,Components(RooArgSet(bkgA)),LineStyle(kDotted)) ;

   modelA.fitTo(dhA) ;
   // M a k e   c o m p o n e n t   b y   n a m e  /   r e g e x p
   // ------------------------------------------------------------
   dhA.plotOn(xframe2A) ;
   // Plot multiple background components specified by name
   modelA.plotOn(xframe2A,Components("modelA,"),LineColor(kBlue)) ;

   // Plot multiple background components specified by regular expression on name
   modelA.plotOn(xframe2A,Components("sig2A"),LineStyle(kDashed),LineColor(kRed)) ;
   modelA.plotOn(xframe2,Components("sig1A"),LineStyle(kDashed),LineColor(kRed)) ;
   modelA.plotOn(xframe2,Components("sig0A"),LineStyle(kDashed),LineColor(kRed)) ;
   // Plot multiple background components specified by multiple regular expressions on name
   modelA.plotOn(xframe2A,Components("bkg2A"),LineStyle(kDashed),LineColor(kViolet)) ;




std::cout<<"Ampiezza piedistallo = "<<meanA0 .getValV()<<" mean1 = "<<meanA1.getValV()<<" "<<" mean2 = "<<meanA2.getValV()<<endl;
std::cout<<"alpha = "<<alphaA.getValV()<<endl;
std::cout<<"spe Ampl = "<<meanA1.getValV() - meanA0.getValV()<<" V / spe = "<<meanA2.getValV()-meanA1.getValV()<<" A"<<endl;
 ///////charge using ROOFIT

 

   // Create 3 Gaussian PDFs sig0(x,mean1,sigma0), sig1(x,mean1,sigma1) and sig2(x,mean2,sigma2) and their parameters
   RooRealVar mean0("mean","mean of gaussians",0,-200,100) ;
   RooRealVar sigma0("sigma","width of gaussians",100,1,150) ;

   RooRealVar mean1("mean1","mean1",750,500,1300) ;
   RooRealVar sigma1("sigma1","sigma1",300,0,600) ;

   RooRealVar mean2("mean2","mean2",1800,1400,2200) ;
   RooRealVar sigma2("sigma2","sigma2",150,100,500) ;

   
   RooGaussian sig0("sig0","Signal component 0",x,mean0,sigma0) ;
   RooGaussian sig1("sig1","Signal component 1",x,mean1,sigma1) ;
   RooGaussian sig2("sig2","Signal component 2",x,mean2,sigma2) ;
   
	// Sum the signal components into a composite signal p.d.f.
   RooRealVar sig1frac("sig1frac","fraction of component 1 in signal",0.9,0.1) ;
   RooAddPdf sig01("sig01","Signal",RooArgList(sig0,sig1),sig1frac) ;
   
   // Sum the signal components into a composite signal p.d.f.
   RooRealVar sig2frac("sig2frac","fraction of component 2 in signal",0.9,0.,1) ;
   RooAddPdf sig("sig","Signal",RooArgList(sig1,sig2),sig2frac) ;

   // Build exponential pdf
   RooRealVar alpha("alpha","alpha",-0.005) ;
   RooExponential bkg2("bkg2","Background 2",x,alpha) ;

   // Sum the background components into a composite background p.d.f.
 //  RooRealVar bkg1frac("sig1frac","fraction of component 1 in background",0.,0.,1.) ;
   RooAddPdf bkg("bkg","Signal",RooArgList(sig0,bkg2),sig1frac) ;

   // Sum the composite signal and background
   RooRealVar bkgfrac("bkgfrac","fraction of background",0.9,0.5,0.9) ;
   RooAddPdf  model("model","g1+g2+a",RooArgList(bkg,sig),bkgfrac) ;

RooDataHist dh("dh","dh",x,t2) ;
sig01.fitTo(dh) ;
   //Ba s i c   p l o t   w i t h   d a t a   a n d   f u l l   p d f
   // ------------------------------------------------------------------------------

   // Generate a data sample of 1000 events in x from model
   //RooDataSet *data = model.generate(x,1000) ;

   // Plot data and complete PDF overlaid
   
   dh.plotOn(xframe) ;


   // M a k e   c o m p o n e n t   b y   o b j e c t   r e f e r e n c e
   // --------------------------------------------------------------------

   model.plotOn(xframe,Components(RooArgSet(bkg)),LineStyle(kDotted)) ;

   model.fitTo(dh) ;
   // M a k e   c o m p o n e n t   b y   n a m e  /   r e g e x p
   // ------------------------------------------------------------
   dh.plotOn(xframe2) ;
   // Plot multiple background components specified by name
   model.plotOn(xframe2,Components("model,"),LineColor(kBlue)) ;

   // Plot multiple background components specified by regular expression on name
   model.plotOn(xframe2,Components("sig2"),LineStyle(kDashed),LineColor(kRed)) ;
   model.plotOn(xframe2,Components("sig1"),LineStyle(kDashed),LineColor(kRed)) ;
   model.plotOn(xframe2,Components("sig0"),LineStyle(kDashed),LineColor(kRed)) ;
   // Plot multiple background components specified by multiple regular expressions on name
   model.plotOn(xframe2,Components("bkg2"),LineStyle(kDashed),LineColor(kViolet)) ;
 //  model.plotOn(xframe2,Components("sig0"),LineStyle(kDotted),LineColor(kViolet)) ;
   // Draw the frame on the canvas
//  TCanvas* c = new TCanvas("rf205_compplot","rf205_compplot",800,400) ;
//gStyle->SetOptFit();
 //  c->Divide(2) ;
 //  c->cd(1) ;xframe->Draw() ;
 //  c->cd(2) ;
 //xframe2->Draw() ;
dh.plotOn(xframe3) ;
 model.plotOn(xframe3,Components("model,"),LineColor(kBlue)) ;


std::cout<<"piedistallo = "<<mean0.getValV()<<" mean1 = "<<mean1.getValV()<<" "<<" mean2 = "<<mean2.getValV()<<endl;
std::cout<<"alpha = "<<alpha.getValV()<<endl;
std::cout<<"spe = "<<mean1.getValV() - mean0.getValV()<<" pVs / spe = "<<mean2.getValV()-mean1.getValV()<<" pVs"<<endl;



////////////////////////////
mean_Q=mean1.getValV() - mean0.getValV();
sigma_Q=sigma2.getValV();
cout<<"SPE peak = "<<mean_A*1000<<" mV\t"<<"sigma Ampl = "<<sigma_A*1000<<" mV\t"<<endl;
cout<<"SPE charge = "<<mean_Q<<" pVs\t"<<"sigma Charge = "<<sigma_Q<<" pVs"<<endl;


}
t1->Draw();
t2->Draw();
 //xframe2->Write();
t1->Write();
t2->Write();

out->Write();
out->Close();

return 0;
}

