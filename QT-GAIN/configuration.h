#ifndef CONFIG_H
#define CONFIG_H

#include <QMainWindow>
#include "ui_mainwindow.h"
#include <iostream>
#include <vector>
#include <QPixmap>
using namespace std;

class configuration{

public:
configuration(Ui::MainWindow *ui):
ui_(ui)
{
    ui->frame->setFrameStyle(QFrame::WinPanel);
    ui->frame_3->setFrameStyle(QFrame::WinPanel);
    ui->frame_6 ->setFrameStyle(QFrame::WinPanel);
 //   ui->frame_7->setFrameStyle(QFrame::WinPanel);

    QPalette palette = ui->frame->palette();
    palette.setColor(QPalette::Background, Qt::blue);//QColor( 0, 0, 240 )
    ui->frame->setPalette( palette );
    ui->frame->setAutoFillBackground( true );

};
vector<QString> getPmtID(){return pmtID;}
vector<double> getSPEconfig(){return speConfig;}
vector<double> getGAINconfig(){return gainConfig;}


void setPmtID();
void setSPEconfig();
void setGAINconfig();
void setDefaultValues();

private slots:









private:
Ui::MainWindow *ui_;
vector<QString> pmtID;
vector<double> speConfig;
vector<double> gainConfig;
vector<QString> vecPath;

//vector<int> pmtID;
};


#endif
