#include "gain.h"
#include <QProcess>
#include <QString>
#include <QFile>
#include <QDir>
#include <QTextStream>

#include <iostream>
#include <sstream>

using namespace std;

/*///////
LEGGE FILE DOVE PRIMA CI SONO I DATI HIGH SIGNAL E POI LOW SIGNAL
DEVO LEGGERE CHARGE HIGH E LA SUA SIGMA ALLE VARIE TENSIONI
E POI LA CARICA DI SPE 
/*////////////////////////////////////////////////
gain::gain (QString ID, vector<double> vec_HV)
{
cout<<"#######INFO: GAIN#######"<<endl;
const char* configfile ="config.dat";

QFile config(configfile);
QTextStream in(&config);
QString tmp, path ,ampl, att , n_spe, n_gain, HV1, HV2,HV3,HV4,HV5 , pdf_path;
if(!config.open(QIODevice::ReadOnly | QIODevice::Text)) cout<<"config file not found!"<<endl;
tmp = in.readLine();
path = in.readLine();
tmp = in.readLine();
ampl = in.readLine();
tmp = in.readLine();
att = in.readLine();

tmp = in.readLine();
n_spe = in.readLine();
tmp = in.readLine();
n_gain = in.readLine();
for(int i=0;i<11;i++) tmp = in.readLine();

pdf_path=in.readLine();
int  n_ev , n_ev_spe;
double amplification_spe, attenuation_gain ,  spe_HV;

vector<double> HV;

amplification_spe=ampl.toDouble();
n_ev_spe=n_spe.toInt();

attenuation_gain=att.toDouble();
n_ev=n_gain.toInt();
/*HV.push_back(HV1.toInt());
HV.push_back(HV2.toInt());
HV.push_back(HV3.toInt());
HV.push_back(HV4.toInt());
HV.push_back(HV5.toInt());
*/



//cout<<"kkkkkkkkkkkk"<<endl;
//for(int i=0;i<HV.size();i++) cout<<HV[i]<<" ";
//cout<<endl;
double peakH, err_peakH, chargeH, err_chargeH, peakL, err_peakL, chargeL, err_chargeL ;

vector<double>   charge_high,  err_charge_high , n_events;
/*
for (int i=0;i<HV.size();i++)cout<<HV[i]<<endl;
cout<<amplification_spe<<endl;
cout<<n_ev_spe<<endl;
cout<<attenuation_gain<<endl;
cout<<n_ev<<endl;
*/

/////////FILE GAIN

path.append("/");
path.append(ID);
pdf_path.append("/");
pdf_path.append(ID);
string file_gain= path.toStdString();

file_gain.append("/gain_output.txt");

ifstream path_gain(file_gain.c_str());
cout<<" path: "<< file_gain<<endl;

string line;
getline(path_gain,line);
//cout<<" path: "<<line<<endl;
double foo, spe=0 , sigma_spe=0;
 if (path_gain.is_open()){		
path_gain>>foo>>foo>>spe>>sigma_spe;
	while(path_gain.good() )

				{

		path_gain>>peakH>>err_peakH>>chargeH>>err_chargeH;
			
		if(path_gain.eof()) break;
		charge_high.push_back(chargeH); 
		err_charge_high.push_back(err_chargeH);
		n_events.push_back(n_ev);

				}
	path_gain.close();
			}

cout<<spe<<" "<<sigma_spe<<" "<<charge_high.back()<<endl;

//double spe =  charge_high.back();
//double sigma_spe = err_charge_high.back();


double err_spe =  sigma_spe/sqrt(n_ev_spe);

for(unsigned int i=0;i<charge_high.size();i++) err_charge_high[i]=err_charge_high[i]/sqrt(n_ev);


double n_pe =(charge_high.front()*attenuation_gain)/(spe/amplification_spe);
cout<<"max = "<<charge_high.back()<<endl;
cout<<"spe = "<<spe<<endl;

n_pe=ceil(n_pe);
cout<<"npe = "<<n_pe<<endl;
double err_npe = n_pe*sqrt(pow(err_charge_high.back()/charge_high.back(),2) + pow(err_spe/spe,2));
err_npe=ceil(err_npe);

cout<<"err npe = "<<err_npe<<endl;
 
 
int dim=charge_high.size();
double g_1[dim]  , err_g[dim] , err_HV[dim] ;



for(int i=0;i<dim;i++){  //parto da 1 per saltare spe
HV.push_back(vec_HV[i+1]);

}
//for(int i=0;i<HV.size();i++){
//cout<<"-------------  "<<HV.at(i)<<endl;}
for( int i=0;i<dim;i++){  
  g_1[i]=charge_high[i]/1.6e-7/50./n_pe*attenuation_gain;
  err_g[i]=g_1[i]*sqrt(pow(err_charge_high[i]/charge_high[i],2)+ pow(err_npe/n_pe,2));
  err_HV[i]=0.;
 }
 
//for (int i=0;i<charge_high.size();i++)cout<<HV[i]<<"\t"<<charge_high[i]<<endl;
 
TCanvas* c1 = new TCanvas("c1","c1",1000,1000);
 
 
 double xx[] = {800, 1500};
 double yy[] = {1e5,5e7};
 TGraph* gs = new TGraph (2,xx,yy);
 TGraphErrors* gain_1 = new TGraphErrors (dim,&HV[0],g_1, err_HV,err_g);
 
 
 gs->Draw("AP");
 gs->GetXaxis()->SetTitle("HV [V]");
 gs->GetYaxis()->SetTitle("Gain");
 
 
 
 gain_1->SetMarkerStyle(20);
 gain_1->SetMarkerColor(2);
 
 
 
 
 TF1 *fgain = new TF1("fgain","[0]*pow(x,[1])",800,1500); 
QDir().mkdir(pdf_path);
QString pdf_filename = pdf_path.append("/gain.pdf");
 QString root_filename = path.append("/gain.root");
 //cout<<root_filename<<endl;
 TFile *f1 = new TFile(root_filename.toStdString().c_str(),"recreate");
 //TPad *p = (TPad *)(c1->cd(1)); ;
 
 //p->SetLogy();
 
 fgain->SetParameters(1e-15, 8);
 //fgain->SetParLimits(2,0., 1e4);
c1->cd();
 gain_1->Draw("AP");
 gain_1->Fit("fgain","","",800,1500);
 //gain_1->SetName("gain");
 
 string title="PMT ID: ";
 title.append(ID.toStdString());
 
 //gain_1->SetTitle("p0 x HV^p1");
 gain_1->SetTitle(title.c_str());
 /*
    gain_1->GetXaxis()->SetMoreLogLabels();
    gain_1->GetYaxis()->SetMoreLogLabels();
    gain_1->GetXaxis()->SetTitle("HV [V]");
    gain_1->GetYaxis()->SetTitle("Gain");
    gain_1->GetXaxis()->SetLabelSize(.05);
    gain_1->GetYaxis()->SetLabelSize(.05);
    gain_1->GetXaxis()->SetTitleSize(.05);
    gain_1->GetYaxis()->SetTitleSize(.05);
    gain_1->GetXaxis()->SetTitleOffset(1.1);
    gain_1->GetYaxis()->SetTitleOffset(.7);
 */
 value.clear();
 


/*
    -NL_H 1*10^5
    -NL_L 5*10^4
    -NL_HH 7*10^5
    -NL_LL 5*10^5
*/
 double p0=fgain->GetParameter(0);
 value.push_back(p0);
 
 double p1=fgain->GetParameter(1);
 value.push_back(p1);
 double chisq = fgain->GetChisquare()/fgain->GetNDF();
 cout << "ChiSquare: "<< chisq<<endl;
 cout <<p0<<" "<<p1<<" "<<endl;

 double aa = (1*pow(10,5))/p0;
 double bb = 1./p1;
 double vv = pow(aa,bb);
 value.push_back(vv);
 cout << "tensione per G=1e5: "<< vv<<endl;
 double vvll = vv;
 
 aa = (5*pow(10,4))/p0;
 bb = 1./p1;
 vv = pow(aa,bb);
 value.push_back(vv);
 cout << "tensione per G=5e4: "<< vv<<endl;
 double vvl = vv;
 
 aa = (7*pow(10,5) )/p0;
 bb = 1./p1;
 vv = pow(aa,bb);
 value.push_back(vv);
 cout << "tensione per G=7e5: "<< vv<<endl;
 double vvh = vv;
 
 aa = (5*pow(10,5) )/p0;
 bb = 1./p1;
 vv = pow(aa,bb);
 value.push_back(vv);
 cout << "tensione per G=5e5: "<< vv<<endl;
 double vvhh = vv;
 
 ofstream outfile;
 outfile.open("Gain.dat",ios_base::app);
 //outfile << ID.toStdString() <<"\t"<< vvl <<"\t"<< vvh <<"\t"<< chisq<<endl;
 outfile << ID.toStdString() <<"\t"<<ceil(vvll) <<"\t"<< ceil(vvl) <<"\t"<< ceil(vvh) <<"\t"<< ceil(vvhh) <<"\t"<<endl;
 

 outfile.close();
 
 //TF1 *func = new TF1("fitgain","[0]*pow(x,[1])",800,1500); 
 //func->SetParameter(0,p0);
 //func->SetParameter(1,p1);

 gain_1->SetMarkerStyle(22);
 gain_1->SetMarkerColor(4);
 
 //func->SetTitle("");
 gain_1->GetXaxis()->SetTitle("HV [V]");
 gain_1->GetYaxis()->SetTitle("Gain");
 gain_1->SetName("g1");
 //p->cd();
  c1->cd();
//    c1->SetLogx();
    c1->SetLogy();
    c1->SetGridx();
    c1->SetGridy();
 gain_1->Write();
//  c1->Print(TString::Format(pdf_filename.toStdString().c_str(),"pdf"));

c1->Update();
c1->Write();
c1->Update();
c1->Draw();
//c1->SaveAs(pdf_filename.toStdString().c_str());
c1->Close();
delete gain_1;
delete fgain;


 f1->Close();

QProcess *process = new QProcess();
QString proc;
 proc="./print_gain.sh ";

proc.append(root_filename);
proc.append(" ");
proc.append(pdf_filename);
cout<<proc.toStdString()<<endl;
//process->start(proc);
}

