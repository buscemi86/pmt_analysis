#include "fill_db.h"
#include <QFile>
#include <QMenu>
#include <fstream>
#include <QDebug>
#include <QSqlQuery>
#include <iostream>
#include <fstream>
#include <iomanip>


fill_db::fill_db(Ui::MainWindow *ui):
ui_(ui)
{

}

void fill_db::show_data(vector<QString> ID ,vector<vector<double>> gain_value,QString RunNumber){
cout<<"#######INFO: FILL_DB::SHOW_DATA#######"<<endl;
IDpmt=ID;
RUN=RunNumber.mid(4,3);

//write HV data in a file
const char* configfile ="config.dat";
string tmp  ;
ifstream config(configfile);
for(int i=0;i<12;i++) getline(config,tmp); //read 12 lines

QString out_file= QString::fromStdString(tmp);

 ofstream myfile;
out_file.append(RunNumber);
out_file.append(".dat");

  myfile.open (out_file.toStdString());
/*	 myfile<<"PMT index "<<
		setiosflags(ios::fixed)<<setprecision(2)<<internal<<setw(18)<<"HV G=6*10^4 "<<
		setiosflags(ios::fixed)<<setprecision(2)<<internal<<setw(18)<<"HV G=7*10^5"<<endl;
for(unsigned int i=0; i<gain_value.size();i++)	{
myfile<<ID[i].toStdString()<<" "
      <<setiosflags(ios::fixed)<<setprecision(2)<<internal<<setw(18)<<(int)gain_value[i][2]<<" "
      <<setiosflags(ios::fixed)<<setprecision(2)<<internal<<setw(18)<<(int)gain_value[i][3];
myfile<<endl;}*/




/*
    -NL_H 1*10^5
    -NL_L 5*10^4
    -NL_HH 7*10^5
    -NL_LL 5*10^5
*/

  myfile<<"PMT index \t HV G=1*10^5 \t HV G=5*10^4 \t HV G=7*10^5 \t HV G=5*10^5"<<endl;
for(unsigned int i=0; i<gain_value.size();i++)	{
myfile<<ID[i].toStdString()<<"\t"<<(int)gain_value[i][2]<<"\t"<<(int)gain_value[i][3]<<"\t"<<(int)gain_value[i][4]<<"\t"<<(int)gain_value[i][5];
myfile<<endl;}
myfile.close();


//set pmt0 gain value
ui_->textEdit_d01->append(ID[0]);
ui_->textEdit_d02->append(QString::number(gain_value[0][0],'g',3));
ui_->textEdit_d03->append(QString::number(gain_value[0][1],'g',3));
ui_->textEdit_d04->append(QString::number((int)gain_value[0][2]));
ui_->textEdit_d05->append(QString::number((int)gain_value[0][3]));
db0.clear();
//db0.push_back(ui_->textEdit_d01->toPlainText().toDouble());
db0.push_back(ui_->textEdit_d02->toPlainText().toDouble());
db0.push_back(ui_->textEdit_d03->toPlainText().toDouble());
db0.push_back(ui_->textEdit_d04->toPlainText().toDouble());
db0.push_back(ui_->textEdit_d05->toPlainText().toDouble());
db0.push_back(0);

//set pmt1 gain value
ui_->textEdit_d11->append(ID[1]);
ui_->textEdit_d12->append(QString::number(gain_value[1][0],'g',3));
ui_->textEdit_d13->append(QString::number(gain_value[1][1],'g',3));
ui_->textEdit_d14->append(QString::number((int)gain_value[1][2]));
ui_->textEdit_d15->append(QString::number((int)gain_value[1][3]));
db1.clear();
//db1.push_back(ui_->textEdit_d11->toPlainText().toDouble());
db1.push_back(ui_->textEdit_d12->toPlainText().toDouble());
db1.push_back(ui_->textEdit_d13->toPlainText().toDouble());
db1.push_back(ui_->textEdit_d14->toPlainText().toDouble());
db1.push_back(ui_->textEdit_d15->toPlainText().toDouble());
db1.push_back(0);

//set pmt2 gain value
ui_->textEdit_d21->append(ID[2]);
ui_->textEdit_d22->append(QString::number(gain_value[2][0],'g',3));
ui_->textEdit_d23->append(QString::number(gain_value[2][1],'g',3));
ui_->textEdit_d24->append(QString::number((int)gain_value[2][2]));
ui_->textEdit_d25->append(QString::number((int)gain_value[2][3]));
db2.clear();
//db2.push_back(ui_->textEdit_d21->toPlainText().toDouble());
db2.push_back(ui_->textEdit_d22->toPlainText().toDouble());
db2.push_back(ui_->textEdit_d23->toPlainText().toDouble());
db2.push_back(ui_->textEdit_d24->toPlainText().toDouble());
db2.push_back(ui_->textEdit_d25->toPlainText().toDouble());
db2.push_back(0);


//set pmt3 gain value
ui_->textEdit_d31->append(ID[3]);
ui_->textEdit_d32->append(QString::number(gain_value[3][0],'g',3));
ui_->textEdit_d33->append(QString::number(gain_value[3][1],'g',3));
ui_->textEdit_d34->append(QString::number((int)gain_value[3][2]));
ui_->textEdit_d35->append(QString::number((int)gain_value[3][3]));
db3.clear();
//db3.push_back(ui_->textEdit_d31->toPlainText().toDouble());
db3.push_back(ui_->textEdit_d32->toPlainText().toDouble());
db3.push_back(ui_->textEdit_d33->toPlainText().toDouble());
db3.push_back(ui_->textEdit_d34->toPlainText().toDouble());
db3.push_back(ui_->textEdit_d35->toPlainText().toDouble());
db3.push_back(0);

//set pmt4 gain value
ui_->textEdit_d41->append(ID[4]);
ui_->textEdit_d42->append(QString::number(gain_value[4][0],'g',3));
ui_->textEdit_d43->append(QString::number(gain_value[4][1],'g',3));
ui_->textEdit_d44->append(QString::number((int)gain_value[4][2]));
ui_->textEdit_d45->append(QString::number((int)gain_value[4][3]));
db4.clear();
//db4.push_back(ui_->textEdit_d41->toPlainText().toDouble());
db4.push_back(ui_->textEdit_d42->toPlainText().toDouble());
db4.push_back(ui_->textEdit_d43->toPlainText().toDouble());
db4.push_back(ui_->textEdit_d44->toPlainText().toDouble());
db4.push_back(ui_->textEdit_d45->toPlainText().toDouble());
db4.push_back(0);


//set pmt5 gain value
ui_->textEdit_d51->append(ID[5]);
ui_->textEdit_d52->append(QString::number(gain_value[5][0],'g',3));
ui_->textEdit_d53->append(QString::number(gain_value[5][1],'g',3));
ui_->textEdit_d54->append(QString::number((int)gain_value[5][2]));
ui_->textEdit_d55->append(QString::number((int)gain_value[5][3]));
db5.clear();
//db5.push_back(ui_->textEdit_d51->toPlainText().toDouble());
db5.push_back(ui_->textEdit_d52->toPlainText().toDouble());
db5.push_back(ui_->textEdit_d53->toPlainText().toDouble());
db5.push_back(ui_->textEdit_d54->toPlainText().toDouble());
db5.push_back(ui_->textEdit_d55->toPlainText().toDouble());
db5.push_back(0);

//set pmt6 gain value
ui_->textEdit_d61->append(ID[6]);
ui_->textEdit_d62->append(QString::number(gain_value[6][0],'g',3));
ui_->textEdit_d63->append(QString::number(gain_value[6][1],'g',3));
ui_->textEdit_d64->append(QString::number((int)gain_value[6][2]));
ui_->textEdit_d65->append(QString::number((int)gain_value[6][3]));
db6.clear();
//db6.push_back(ui_->textEdit_d61->toPlainText().toDouble());
db6.push_back(ui_->textEdit_d62->toPlainText().toDouble());
db6.push_back(ui_->textEdit_d63->toPlainText().toDouble());
db6.push_back(ui_->textEdit_d64->toPlainText().toDouble());
db6.push_back(ui_->textEdit_d65->toPlainText().toDouble());
db6.push_back(0);


//set pmt7 gain value
ui_->textEdit_d71->append(ID[7]);
ui_->textEdit_d72->append(QString::number(gain_value[7][0],'g',3));
ui_->textEdit_d73->append(QString::number(gain_value[7][1],'g',3));
ui_->textEdit_d74->append(QString::number((int)gain_value[7][2]));
ui_->textEdit_d75->append(QString::number((int)gain_value[7][3]));
db7.clear();
//db7.push_back(ui_->textEdit_d71->toPlainText().toDouble());
db7.push_back(ui_->textEdit_d72->toPlainText().toDouble());
db7.push_back(ui_->textEdit_d73->toPlainText().toDouble());
db7.push_back(ui_->textEdit_d74->toPlainText().toDouble());
db7.push_back(ui_->textEdit_d75->toPlainText().toDouble());
db7.push_back(0);


//set pmt8 gain value
ui_->textEdit_d81->append(ID[8]);
ui_->textEdit_d82->append(QString::number(gain_value[8][0],'g',3));
ui_->textEdit_d83->append(QString::number(gain_value[8][1],'g',3));
ui_->textEdit_d84->append(QString::number((int)gain_value[8][2]));
ui_->textEdit_d85->append(QString::number((int)gain_value[8][3]));
db8.clear();
//db8.push_back(ui_->textEdit_d81->toPlainText().toDouble());
db8.push_back(ui_->textEdit_d82->toPlainText().toDouble());
db8.push_back(ui_->textEdit_d83->toPlainText().toDouble());
db8.push_back(ui_->textEdit_d84->toPlainText().toDouble());
db8.push_back(ui_->textEdit_d85->toPlainText().toDouble());
db8.push_back(0);


//set pmt9 gain value
ui_->textEdit_d91->append(ID[9]);
ui_->textEdit_d92->append(QString::number(gain_value[9][0],'g',3));
ui_->textEdit_d93->append(QString::number(gain_value[9][1],'g',3));
ui_->textEdit_d94->append(QString::number((int)gain_value[9][2]));
ui_->textEdit_d95->append(QString::number((int)gain_value[9][3]));
db9.clear();
//db9.push_back(ui_->textEdit_d91->toPlainText().toDouble());
db9.push_back(ui_->textEdit_d92->toPlainText().toDouble());
db9.push_back(ui_->textEdit_d93->toPlainText().toDouble());
db9.push_back(ui_->textEdit_d94->toPlainText().toDouble());
db9.push_back(ui_->textEdit_d95->toPlainText().toDouble());
db9.push_back(0);



//set pmt10 gain value
ui_->textEdit_d101->append(ID[10]);
ui_->textEdit_d102->append(QString::number(gain_value[10][0],'g',3));
ui_->textEdit_d103->append(QString::number(gain_value[10][1],'g',3));
ui_->textEdit_d104->append(QString::number((int)gain_value[10][2]));
ui_->textEdit_d105->append(QString::number((int)gain_value[10][3]));
db10.clear();
//db10.push_back(ui_->textEdit_d101->toPlainText().toDouble());
db10.push_back(ui_->textEdit_d102->toPlainText().toDouble());
db10.push_back(ui_->textEdit_d103->toPlainText().toDouble());
db10.push_back(ui_->textEdit_d104->toPlainText().toDouble());
db10.push_back(ui_->textEdit_d105->toPlainText().toDouble());
db10.push_back(0);


//set pmt11 gain value
ui_->textEdit_d111->append(ID[11]);
ui_->textEdit_d112->append(QString::number(gain_value[11][0],'g',3));
ui_->textEdit_d113->append(QString::number(gain_value[11][1],'g',3));
ui_->textEdit_d114->append(QString::number((int)gain_value[11][2]));
ui_->textEdit_d115->append(QString::number((int)gain_value[11][3]));
db11.clear();
//db11.push_back(ui_->textEdit_d111->toPlainText().toDouble());
db11.push_back(ui_->textEdit_d112->toPlainText().toDouble());
db11.push_back(ui_->textEdit_d113->toPlainText().toDouble());
db11.push_back(ui_->textEdit_d114->toPlainText().toDouble());
db11.push_back(ui_->textEdit_d115->toPlainText().toDouble());
db11.push_back(0);


//set pmt12 gain value
ui_->textEdit_d121->append(ID[12]);
ui_->textEdit_d122->append(QString::number(gain_value[12][0],'g',3));
ui_->textEdit_d123->append(QString::number(gain_value[12][1],'g',3));
ui_->textEdit_d124->append(QString::number((int)gain_value[12][2]));
ui_->textEdit_d125->append(QString::number((int)gain_value[12][3]));
db12.clear();
//db12.push_back(ui_->textEdit_d121->toPlainText().toDouble());
db12.push_back(ui_->textEdit_d122->toPlainText().toDouble());
db12.push_back(ui_->textEdit_d123->toPlainText().toDouble());
db12.push_back(ui_->textEdit_d124->toPlainText().toDouble());
db12.push_back(ui_->textEdit_d125->toPlainText().toDouble());
db12.push_back(0);


//set pmt13 gain value
ui_->textEdit_d131->append(ID[13]);
ui_->textEdit_d132->append(QString::number(gain_value[13][0],'g',3));
ui_->textEdit_d133->append(QString::number(gain_value[13][1],'g',3));
ui_->textEdit_d134->append(QString::number((int)gain_value[13][2]));
ui_->textEdit_d135->append(QString::number((int)gain_value[13][3]));
db13.clear();
//db13.push_back(ui_->textEdit_d131->toPlainText().toDouble());
db13.push_back(ui_->textEdit_d132->toPlainText().toDouble());
db13.push_back(ui_->textEdit_d133->toPlainText().toDouble());
db13.push_back(ui_->textEdit_d134->toPlainText().toDouble());
db13.push_back(ui_->textEdit_d135->toPlainText().toDouble());
db13.push_back(0);


//set pmt14 gain value
ui_->textEdit_d141->append(ID[14]);
ui_->textEdit_d142->append(QString::number(gain_value[14][0],'g',3));
ui_->textEdit_d143->append(QString::number(gain_value[14][1],'g',3));
ui_->textEdit_d144->append(QString::number((int)gain_value[14][2]));
ui_->textEdit_d145->append(QString::number((int)gain_value[14][3]));
db14.clear();
//db14.push_back(ui_->textEdit_d141->toPlainText().toDouble());
db14.push_back(ui_->textEdit_d142->toPlainText().toDouble());
db14.push_back(ui_->textEdit_d143->toPlainText().toDouble());
db14.push_back(ui_->textEdit_d144->toPlainText().toDouble());
db14.push_back(ui_->textEdit_d145->toPlainText().toDouble());
db14.push_back(0);


//set pmt15 gain value
ui_->textEdit_d151->append(ID[15]);
ui_->textEdit_d152->append(QString::number(gain_value[15][0],'g',3));
ui_->textEdit_d153->append(QString::number(gain_value[15][1],'g',3));
ui_->textEdit_d154->append(QString::number((int)gain_value[15][2]));
ui_->textEdit_d155->append(QString::number((int)gain_value[15][3]));
db15.clear();
//db15.push_back(ui_->textEdit_d151->toPlainText().toDouble());
db15.push_back(ui_->textEdit_d152->toPlainText().toDouble());
db15.push_back(ui_->textEdit_d153->toPlainText().toDouble());
db15.push_back(ui_->textEdit_d154->toPlainText().toDouble());
db15.push_back(ui_->textEdit_d155->toPlainText().toDouble());
db15.push_back(0);


}



int fill_db::connect(){
cout<<"#######INFO: FILL_DB::CONNECT"<<endl;
QString user = ui_->lineEdit_user->text();

QString password = ui_->lineEdit_psw->text();

const char* configfile ="db_config.dat";


string tmp  ;
ifstream config(configfile);

getline(config,tmp); //1st line
getline(config,tmp); //2nd line
QString hostname= QString::fromStdString(tmp);
//qDebug()<<hostname;
getline(config,tmp); //3rd line
getline(config,tmp); //4th line
QString dbName= QString::fromStdString(tmp);


        db = QSqlDatabase::addDatabase("QMYSQL");
        db.setHostName(hostname);
        db.setDatabaseName(dbName);
        db.setUserName(user);
        db.setPassword(password);
        int ok = db.open();
    
   		if(ok) {cout<<"Connected to Database"<<endl; //return 1 if connected
			ui_->textEdit_dbStatus->clear();
			ui_->textEdit_dbStatus->append("Connected");
			return ok;
			}
	
		else	{cout<<"Connection failed!"<<endl;
			return 0;}

			}


void fill_db::disconnect(){
cout<<"#######INFO: FILL_DB::DISCONNECT"<<endl;
db.close();
ui_->textEdit_dbStatus->clear();
ui_->textEdit_dbStatus->append("Disconnected");
}

void fill_db::clear(){
    //set pmt0 gain value
    ui_->textEdit_d01->clear();
    ui_->textEdit_d02->clear();
    ui_->textEdit_d03->clear();
    ui_->textEdit_d04->clear();
    ui_->textEdit_d05->clear();
    ui_->textEdit_d06->clear();
    db0.clear();


    //set pmt1 gain value
    ui_->textEdit_d11->clear();
    ui_->textEdit_d12->clear();
    ui_->textEdit_d13->clear();
    ui_->textEdit_d14->clear();
    ui_->textEdit_d15->clear();
    ui_->textEdit_d16->clear();
    db1.clear();

    //set pmt2 gain value
    ui_->textEdit_d21->clear();
    ui_->textEdit_d22->clear();
    ui_->textEdit_d23->clear();
    ui_->textEdit_d24->clear();
    ui_->textEdit_d25->clear();
    ui_->textEdit_d26->clear();  
    db2.clear(); 


    //set pmt3 gain value
    ui_->textEdit_d31->clear();
    ui_->textEdit_d32->clear();
    ui_->textEdit_d33->clear();
    ui_->textEdit_d34->clear();
    ui_->textEdit_d35->clear();
    ui_->textEdit_d36->clear();
    db3.clear();

    //set pmt4 gain value
    ui_->textEdit_d41->clear();
    ui_->textEdit_d42->clear();
    ui_->textEdit_d43->clear();
    ui_->textEdit_d44->clear();
    ui_->textEdit_d45->clear();
    ui_->textEdit_d46->clear();    
    db4.clear();


    //set pmt5 gain value
    ui_->textEdit_d51->clear();
    ui_->textEdit_d52->clear();
    ui_->textEdit_d53->clear();
    ui_->textEdit_d54->clear();
    ui_->textEdit_d55->clear();
    ui_->textEdit_d56->clear();
    db5.clear();

    //set pmt6 gain value
    ui_->textEdit_d61->clear();
    ui_->textEdit_d62->clear();
    ui_->textEdit_d63->clear();
    ui_->textEdit_d64->clear();
    ui_->textEdit_d65->clear();
    ui_->textEdit_d66->clear();
    db6.clear();



    //set pmt7 gain value
    ui_->textEdit_d71->clear();
    ui_->textEdit_d72->clear();
    ui_->textEdit_d73->clear();
    ui_->textEdit_d74->clear();
    ui_->textEdit_d75->clear();
    ui_->textEdit_d76->clear();
    db7.clear();


    //set pmt8 gain value
    ui_->textEdit_d81->clear();
    ui_->textEdit_d82->clear();
    ui_->textEdit_d83->clear();
    ui_->textEdit_d84->clear();
    ui_->textEdit_d85->clear();
    ui_->textEdit_d86->clear();
    db8.clear();

    //set pmt9 gain value
    ui_->textEdit_d91->clear();
    ui_->textEdit_d92->clear();
    ui_->textEdit_d93->clear();
    ui_->textEdit_d94->clear();
    ui_->textEdit_d95->clear();
    ui_->textEdit_d96->clear();
    db9.clear();

    //set pmt10 gain value
    ui_->textEdit_d101->clear();
    ui_->textEdit_d102->clear();
    ui_->textEdit_d103->clear();
    ui_->textEdit_d104->clear();
    ui_->textEdit_d105->clear();
    ui_->textEdit_d106->clear();
    db10.clear();

    //set pmt11 gain value
    ui_->textEdit_d111->clear();
    ui_->textEdit_d112->clear();
    ui_->textEdit_d113->clear();
    ui_->textEdit_d114->clear();
    ui_->textEdit_d115->clear();
    ui_->textEdit_d116->clear();
    db11.clear();


    //set pmt12 gain value
    ui_->textEdit_d121->clear();
    ui_->textEdit_d122->clear();
    ui_->textEdit_d123->clear();
    ui_->textEdit_d124->clear();
    ui_->textEdit_d125->clear();
    ui_->textEdit_d126->clear();
    db12.clear();

    //set pmt13 gain value
    ui_->textEdit_d131->clear();
    ui_->textEdit_d132->clear();
    ui_->textEdit_d133->clear();
    ui_->textEdit_d134->clear();
    ui_->textEdit_d135->clear();
    ui_->textEdit_d136->clear();
    db13.clear();

    //set pmt14 gain value
    ui_->textEdit_d141->clear();
    ui_->textEdit_d142->clear();
    ui_->textEdit_d143->clear();
    ui_->textEdit_d144->clear();
    ui_->textEdit_d145->clear();
    ui_->textEdit_d146->clear();
    db14.clear();

    //set pmt15 gain value
    ui_->textEdit_d151->clear();
    ui_->textEdit_d152->clear();
    ui_->textEdit_d153->clear();
    ui_->textEdit_d154->clear();
    ui_->textEdit_d155->clear();
    ui_->textEdit_d156->clear();
    db15.clear();



    }

void fill_db::store_all_data(){
cout<<"#######INFO: FILL_DB::STORE_ALL_DATA"<<endl;
const char* configfile ="config.dat";
string tmp  ;
ifstream config(configfile);
for(unsigned int i=0;i<24;i++) getline(config,tmp); //read 12 lines
QString webpage= QString::fromStdString(tmp);
vector<QString> gain_web , linH_web , linL_web;



vector<vector<double>> vec_db;
vec_db.push_back(db0);
vec_db.push_back(db1);
vec_db.push_back(db2);
vec_db.push_back(db3);
vec_db.push_back(db4);
vec_db.push_back(db5);
vec_db.push_back(db6);
vec_db.push_back(db7);
vec_db.push_back(db8);
vec_db.push_back(db9);
vec_db.push_back(db10);
vec_db.push_back(db11);
vec_db.push_back(db12);
vec_db.push_back(db13);
vec_db.push_back(db14);
vec_db.push_back(db15);


for(unsigned int i=0; i<vec_db.size();i++){
QString wb = webpage;

wb.append("/");
wb.append(IDpmt[i]);
wb.append("/");

QString wb1 = wb;
QString wb2 = wb;
QString wb3 = wb;


gain_web.push_back(wb1.append("gain.pdf"));
linH_web.push_back(wb2.append("linearity_High.pdf"));
linL_web.push_back(wb3.append("linearity_Low.pdf"));


}


QString queryString;
QTextStream queryStream(&queryString);
//QSqlQuery query( "" );

for(unsigned int i=0; i<vec_db.size();i++){


queryString.clear();
queryStream<<"INSERT INTO pmt  (RUN , ID , p0 , p1 , HV1 , HV2 , DCR, pdf_gain  ) VALUES ( '"<< RUN  << "','"<< IDpmt[i] << "','" << vec_db[i][0] << "','" << vec_db[i][1] << "','" << vec_db[i][2] << "','" << vec_db[i][3] << "','" << vec_db[i][4] <<"','"<< gain_web[i] <<"');";




//qDebug()<<*queryStream.string();
QSqlQuery query( *queryStream.string() );
}
} 

void fill_db::store_data(vector<int> flags){
cout<<"#######INFO: FILL_DB::STORE_DATA"<<endl;
const char* configfile ="config.dat";
string tmp  ;
ifstream config(configfile);
for(unsigned int i=0;i<24;i++) getline(config,tmp); //read 12 lines
QString webpage= QString::fromStdString(tmp);
vector<QString> gain_web ;

for(unsigned int i=0; i<16;i++){
QString wb = webpage;

wb.append("/");
wb.append(IDpmt[i]);
wb.append("/");

QString wb1 = wb;


gain_web.push_back(wb1.append("gain.pdf"));


}

//QSqlQuery query("INSERT INTO pmt VALUES [ID , p0 , p1 , HV1 , HV2] ( db0[0], db0[1], db0[2],db0[3],db0[4]" );

vector<vector<double>> vec_db;
vector<QString> selID;
vector<QString> gainW;
vector<QString> linHW;
vector<QString> linLW;

//for(int i=0;i<flags.size();i++) cout<<flags[i]<<" "<<IDpmt[i].toStdString()<<endl;
if(flags[0]==1)  {vec_db.push_back(db0);  selID.push_back(IDpmt[0]  ); gainW.push_back(gain_web[0] );}
if(flags[1]==1)  {vec_db.push_back(db1);  selID.push_back(IDpmt[1]  ); gainW.push_back(gain_web[1] );}
if(flags[2]==1)  {vec_db.push_back(db2);  selID.push_back(IDpmt[2]  ); gainW.push_back(gain_web[2] );}
if(flags[3]==1)  {vec_db.push_back(db3);  selID.push_back(IDpmt[3]  ); gainW.push_back(gain_web[3] );}
if(flags[4]==1)  {vec_db.push_back(db4);  selID.push_back(IDpmt[4]  ); gainW.push_back(gain_web[4] );}
if(flags[5]==1)  {vec_db.push_back(db5);  selID.push_back(IDpmt[5]  ); gainW.push_back(gain_web[5] );}
if(flags[6]==1)  {vec_db.push_back(db6);  selID.push_back(IDpmt[6]  ); gainW.push_back(gain_web[6] );}
if(flags[7]==1)  {vec_db.push_back(db7);  selID.push_back(IDpmt[7]  ); gainW.push_back(gain_web[7] );}
if(flags[8]==1)  {vec_db.push_back(db8);  selID.push_back(IDpmt[8]  ); gainW.push_back(gain_web[8] );}
if(flags[9]==1)  {vec_db.push_back(db9);  selID.push_back(IDpmt[9]  ); gainW.push_back(gain_web[9] );}
if(flags[10]==1) {vec_db.push_back(db10); selID.push_back(IDpmt[10] ); gainW.push_back(gain_web[10]);}
if(flags[11]==1) {vec_db.push_back(db11); selID.push_back(IDpmt[11] ); gainW.push_back(gain_web[11]);}
if(flags[12]==1) {vec_db.push_back(db12); selID.push_back(IDpmt[12] ); gainW.push_back(gain_web[12]);}
if(flags[13]==1) {vec_db.push_back(db13); selID.push_back(IDpmt[13] ); gainW.push_back(gain_web[13]);}
if(flags[14]==1) {vec_db.push_back(db14); selID.push_back(IDpmt[14] ); gainW.push_back(gain_web[14]);}
if(flags[15]==1) {vec_db.push_back(db15); selID.push_back(IDpmt[15] ); gainW.push_back(gain_web[15]);}

//cout<<vec_db.size()<<endl;
//cout<<selID.size()<<endl;

QString queryString;
QTextStream queryStream(&queryString);
//QSqlQuery query( "" );

for(unsigned int i=0; i<vec_db.size();i++){
queryString.clear();
queryStream<<"INSERT INTO pmt  (RUN , ID , p0 , p1 , HV1 , HV2 , DCR, pdf_gain ) VALUES ( '"<< RUN << "','"<< selID[i] << "','" << vec_db[i][0] << "','" << vec_db[i][1] << "','" << vec_db[i][2] << "','" << vec_db[i][3] << "','" << vec_db[i][4] <<"','"<< gainW[i] <<"');";


//qDebug()<<*queryStream.string();
QSqlQuery query( *queryStream.string() );


}



}

void fill_db::load_data(){
QString index = ui_->lineEdit_ID->text();

ui_->textEdit_p0->clear();
ui_->textEdit_p1->clear();
ui_->textEdit_HV1->clear();
ui_->textEdit_HV2->clear();
ui_->textEdit_DCR->clear();
ui_->textEdit_RUN->clear();

QString queryString;
QTextStream queryStream(&queryString);

queryStream<<"SELECT p0 FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query1(*queryStream.string());
query1.next();
QString p0 = QString::number(query1.value(0).toDouble(),'e',2);
ui_->textEdit_p0->append(p0);
queryString.clear();

queryStream<<"SELECT p1 FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query2(*queryStream.string());
query2.next();
QString p1 = QString::number(query2.value(0).toDouble(),'g',2);
ui_->textEdit_p1->append(p1);
queryString.clear();


queryStream<<"SELECT HV1 FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query3(*queryStream.string());
query3.next();
QString HV1 = query3.value(0).toString();
ui_->textEdit_HV1->append(HV1);
queryString.clear();


queryStream<<"SELECT HV2 FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query4(*queryStream.string());
query4.next();
QString HV2 = query4.value(0).toString();
ui_->textEdit_HV2->append(HV2);
queryString.clear();

queryStream<<"SELECT DCR FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query5(*queryStream.string());
query5.next();
QString DCR = query5.value(0).toString();
ui_->textEdit_DCR->append(DCR);
queryString.clear();

queryStream<<"SELECT RUN FROM pmt WHERE ID = '"<< index << "';"; 
//qDebug()<<*queryStream.string();
QSqlQuery query6(*queryStream.string());
query6.next();
QString RUN = query6.value(0).toString();
ui_->textEdit_RUN->append(RUN);
queryString.clear();

}

void fill_db::show_dcr(vector<QString> ID ,vector<vector<double>> gain_value,QString RunNumber){
cout<<"#######INFO: FILL_DB::SHOW_DCR"<<endl;
IDpmt=ID;

//write HV data in a file
const char* configfile ="config.dat";
string tmp  ;
ifstream config(configfile);
for(int i=0;i<12;i++) getline(config,tmp); //read 12 lines

QString out_file= QString::fromStdString(tmp);

 ofstream myfile;
out_file.append(RunNumber);
out_file.append(".dat");

  myfile.open (out_file.toStdString());


  myfile<<"PMT index \t HV G=6*10^4 \t HV G=7*10^5"<<endl;
for(unsigned int i=0; i<gain_value.size();i++)	{
myfile<<ID[i].toStdString()<<"\t"<<(int)gain_value[i][2]<<"\t"<<(int)gain_value[i][3];
myfile<<endl;}
myfile.close();


//set pmt0 gain value
ui_->textEdit_d01->append(ID[0]);
ui_->textEdit_d02->append(QString::number(gain_value[0][0],'g',3));
ui_->textEdit_d03->append(QString::number(gain_value[0][1],'g',3));
ui_->textEdit_d04->append(QString::number((int)gain_value[0][2]));
ui_->textEdit_d05->append(QString::number((int)gain_value[0][3]));
ui_->textEdit_d06->append(QString::number((int)DCR[0]));
db0.clear();
//db0.push_back(ui_->textEdit_d01->toPlainText().toDouble());
db0.push_back(ui_->textEdit_d02->toPlainText().toDouble());
db0.push_back(ui_->textEdit_d03->toPlainText().toDouble());
db0.push_back(ui_->textEdit_d04->toPlainText().toDouble());
db0.push_back(ui_->textEdit_d05->toPlainText().toDouble());
db0.push_back(ui_->textEdit_d06->toPlainText().toDouble());

//set pmt1 gain value
ui_->textEdit_d11->append(ID[1]);
ui_->textEdit_d12->append(QString::number(gain_value[1][0],'g',3));
ui_->textEdit_d13->append(QString::number(gain_value[1][1],'g',3));
ui_->textEdit_d14->append(QString::number((int)gain_value[1][2]));
ui_->textEdit_d15->append(QString::number((int)gain_value[1][3]));
ui_->textEdit_d16->append(QString::number((int)DCR[1]));
db1.clear();
//db1.push_back(ui_->textEdit_d11->toPlainText().toDouble());
db1.push_back(ui_->textEdit_d12->toPlainText().toDouble());
db1.push_back(ui_->textEdit_d13->toPlainText().toDouble());
db1.push_back(ui_->textEdit_d14->toPlainText().toDouble());
db1.push_back(ui_->textEdit_d15->toPlainText().toDouble());
db1.push_back(ui_->textEdit_d16->toPlainText().toDouble());
//set pmt2 gain value
ui_->textEdit_d21->append(ID[2]);
ui_->textEdit_d22->append(QString::number(gain_value[2][0],'g',3));
ui_->textEdit_d23->append(QString::number(gain_value[2][1],'g',3));
ui_->textEdit_d24->append(QString::number((int)gain_value[2][2]));
ui_->textEdit_d25->append(QString::number((int)gain_value[2][3]));
ui_->textEdit_d26->append(QString::number((int)DCR[2]));
db2.clear();
//db2.push_back(ui_->textEdit_d21->toPlainText().toDouble());
db2.push_back(ui_->textEdit_d22->toPlainText().toDouble());
db2.push_back(ui_->textEdit_d23->toPlainText().toDouble());
db2.push_back(ui_->textEdit_d24->toPlainText().toDouble());
db2.push_back(ui_->textEdit_d25->toPlainText().toDouble());
db2.push_back(ui_->textEdit_d26->toPlainText().toDouble());



//set pmt3 gain value
ui_->textEdit_d31->append(ID[3]);
ui_->textEdit_d32->append(QString::number(gain_value[3][0],'g',3));
ui_->textEdit_d33->append(QString::number(gain_value[3][1],'g',3));
ui_->textEdit_d34->append(QString::number((int)gain_value[3][2]));
ui_->textEdit_d35->append(QString::number((int)gain_value[3][3]));
ui_->textEdit_d36->append(QString::number((int)DCR[3]));
db3.clear();
//db3.push_back(ui_->textEdit_d31->toPlainText().toDouble());
db3.push_back(ui_->textEdit_d32->toPlainText().toDouble());
db3.push_back(ui_->textEdit_d33->toPlainText().toDouble());
db3.push_back(ui_->textEdit_d34->toPlainText().toDouble());
db3.push_back(ui_->textEdit_d35->toPlainText().toDouble());
db3.push_back(ui_->textEdit_d36->toPlainText().toDouble());
//set pmt4 gain value
ui_->textEdit_d41->append(ID[4]);
ui_->textEdit_d42->append(QString::number(gain_value[4][0],'g',3));
ui_->textEdit_d43->append(QString::number(gain_value[4][1],'g',3));
ui_->textEdit_d44->append(QString::number((int)gain_value[4][2]));
ui_->textEdit_d45->append(QString::number((int)gain_value[4][3]));
ui_->textEdit_d46->append(QString::number((int)DCR[4]));
db4.clear();
//db4.push_back(ui_->textEdit_d41->toPlainText().toDouble());
db4.push_back(ui_->textEdit_d42->toPlainText().toDouble());
db4.push_back(ui_->textEdit_d43->toPlainText().toDouble());
db4.push_back(ui_->textEdit_d44->toPlainText().toDouble());
db4.push_back(ui_->textEdit_d45->toPlainText().toDouble());
db4.push_back(ui_->textEdit_d46->toPlainText().toDouble());

//set pmt5 gain value
ui_->textEdit_d51->append(ID[5]);
ui_->textEdit_d52->append(QString::number(gain_value[5][0],'g',3));
ui_->textEdit_d53->append(QString::number(gain_value[5][1],'g',3));
ui_->textEdit_d54->append(QString::number((int)gain_value[5][2]));
ui_->textEdit_d55->append(QString::number((int)gain_value[5][3]));
ui_->textEdit_d56->append(QString::number((int)DCR[5]));
db5.clear();
//db5.push_back(ui_->textEdit_d51->toPlainText().toDouble());
db5.push_back(ui_->textEdit_d52->toPlainText().toDouble());
db5.push_back(ui_->textEdit_d53->toPlainText().toDouble());
db5.push_back(ui_->textEdit_d54->toPlainText().toDouble());
db5.push_back(ui_->textEdit_d55->toPlainText().toDouble());
db5.push_back(ui_->textEdit_d56->toPlainText().toDouble());
//set pmt6 gain value
ui_->textEdit_d61->append(ID[6]);
ui_->textEdit_d62->append(QString::number(gain_value[6][0],'g',3));
ui_->textEdit_d63->append(QString::number(gain_value[6][1],'g',3));
ui_->textEdit_d64->append(QString::number((int)gain_value[6][2]));
ui_->textEdit_d65->append(QString::number((int)gain_value[6][3]));
ui_->textEdit_d66->append(QString::number((int)DCR[6]));
db6.clear();
//db6.push_back(ui_->textEdit_d61->toPlainText().toDouble());
db6.push_back(ui_->textEdit_d62->toPlainText().toDouble());
db6.push_back(ui_->textEdit_d63->toPlainText().toDouble());
db6.push_back(ui_->textEdit_d64->toPlainText().toDouble());
db6.push_back(ui_->textEdit_d65->toPlainText().toDouble());
db6.push_back(ui_->textEdit_d66->toPlainText().toDouble());

//set pmt7 gain value
ui_->textEdit_d71->append(ID[7]);
ui_->textEdit_d72->append(QString::number(gain_value[7][0],'g',3));
ui_->textEdit_d73->append(QString::number(gain_value[7][1],'g',3));
ui_->textEdit_d74->append(QString::number((int)gain_value[7][2]));
ui_->textEdit_d75->append(QString::number((int)gain_value[7][3]));
ui_->textEdit_d76->append(QString::number((int)DCR[7]));
db7.clear();
//db7.push_back(ui_->textEdit_d71->toPlainText().toDouble());
db7.push_back(ui_->textEdit_d72->toPlainText().toDouble());
db7.push_back(ui_->textEdit_d73->toPlainText().toDouble());
db7.push_back(ui_->textEdit_d74->toPlainText().toDouble());
db7.push_back(ui_->textEdit_d75->toPlainText().toDouble());
db7.push_back(ui_->textEdit_d76->toPlainText().toDouble());

//set pmt8 gain value
ui_->textEdit_d81->append(ID[8]);
ui_->textEdit_d82->append(QString::number(gain_value[8][0],'g',3));
ui_->textEdit_d83->append(QString::number(gain_value[8][1],'g',3));
ui_->textEdit_d84->append(QString::number((int)gain_value[8][2]));
ui_->textEdit_d85->append(QString::number((int)gain_value[8][3]));
ui_->textEdit_d86->append(QString::number((int)DCR[8]));
db8.clear();
//db8.push_back(ui_->textEdit_d81->toPlainText().toDouble());
db8.push_back(ui_->textEdit_d82->toPlainText().toDouble());
db8.push_back(ui_->textEdit_d83->toPlainText().toDouble());
db8.push_back(ui_->textEdit_d84->toPlainText().toDouble());
db8.push_back(ui_->textEdit_d85->toPlainText().toDouble());
db8.push_back(ui_->textEdit_d86->toPlainText().toDouble());
//set pmt9 gain value
ui_->textEdit_d91->append(ID[9]);
ui_->textEdit_d92->append(QString::number(gain_value[9][0],'g',3));
ui_->textEdit_d93->append(QString::number(gain_value[9][1],'g',3));
ui_->textEdit_d94->append(QString::number((int)gain_value[9][2]));
ui_->textEdit_d95->append(QString::number((int)gain_value[9][3]));
ui_->textEdit_d96->append(QString::number((int)DCR[9]));
db9.clear();
//db9.push_back(ui_->textEdit_d91->toPlainText().toDouble());
db9.push_back(ui_->textEdit_d92->toPlainText().toDouble());
db9.push_back(ui_->textEdit_d93->toPlainText().toDouble());
db9.push_back(ui_->textEdit_d94->toPlainText().toDouble());
db9.push_back(ui_->textEdit_d95->toPlainText().toDouble());
db9.push_back(ui_->textEdit_d96->toPlainText().toDouble());
//set pmt10 gain value
ui_->textEdit_d101->append(ID[10]);
ui_->textEdit_d102->append(QString::number(gain_value[10][0],'g',3));
ui_->textEdit_d103->append(QString::number(gain_value[10][1],'g',3));
ui_->textEdit_d104->append(QString::number((int)gain_value[10][2]));
ui_->textEdit_d105->append(QString::number((int)gain_value[10][3]));
ui_->textEdit_d106->append(QString::number((int)DCR[10]));
db10.clear();
//db10.push_back(ui_->textEdit_d101->toPlainText().toDouble());
db10.push_back(ui_->textEdit_d102->toPlainText().toDouble());
db10.push_back(ui_->textEdit_d103->toPlainText().toDouble());
db10.push_back(ui_->textEdit_d104->toPlainText().toDouble());
db10.push_back(ui_->textEdit_d105->toPlainText().toDouble());
db10.push_back(ui_->textEdit_d106->toPlainText().toDouble());
//set pmt11 gain value
ui_->textEdit_d111->append(ID[11]);
ui_->textEdit_d112->append(QString::number(gain_value[11][0],'g',3));
ui_->textEdit_d113->append(QString::number(gain_value[11][1],'g',3));
ui_->textEdit_d114->append(QString::number((int)gain_value[11][2]));
ui_->textEdit_d115->append(QString::number((int)gain_value[11][3]));
ui_->textEdit_d116->append(QString::number((int)DCR[11]));
db11.clear();
//db11.push_back(ui_->textEdit_d111->toPlainText().toDouble());
db11.push_back(ui_->textEdit_d112->toPlainText().toDouble());
db11.push_back(ui_->textEdit_d113->toPlainText().toDouble());
db11.push_back(ui_->textEdit_d114->toPlainText().toDouble());
db11.push_back(ui_->textEdit_d115->toPlainText().toDouble());
db11.push_back(ui_->textEdit_d116->toPlainText().toDouble());

//set pmt12 gain value
ui_->textEdit_d121->append(ID[12]);
ui_->textEdit_d122->append(QString::number(gain_value[12][0],'g',3));
ui_->textEdit_d123->append(QString::number(gain_value[12][1],'g',3));
ui_->textEdit_d124->append(QString::number((int)gain_value[12][2]));
ui_->textEdit_d125->append(QString::number((int)gain_value[12][3]));
ui_->textEdit_d126->append(QString::number((int)DCR[12]));
db12.clear();
//db12.push_back(ui_->textEdit_d121->toPlainText().toDouble());
db12.push_back(ui_->textEdit_d122->toPlainText().toDouble());
db12.push_back(ui_->textEdit_d123->toPlainText().toDouble());
db12.push_back(ui_->textEdit_d124->toPlainText().toDouble());
db12.push_back(ui_->textEdit_d125->toPlainText().toDouble());
db12.push_back(ui_->textEdit_d126->toPlainText().toDouble());
//set pmt13 gain value
ui_->textEdit_d131->append(ID[13]);
ui_->textEdit_d132->append(QString::number(gain_value[13][0],'g',3));
ui_->textEdit_d133->append(QString::number(gain_value[13][1],'g',3));
ui_->textEdit_d134->append(QString::number((int)gain_value[13][2]));
ui_->textEdit_d135->append(QString::number((int)gain_value[13][3]));
ui_->textEdit_d136->append(QString::number((int)DCR[13]));
db13.clear();
//db13.push_back(ui_->textEdit_d131->toPlainText().toDouble());
db13.push_back(ui_->textEdit_d132->toPlainText().toDouble());
db13.push_back(ui_->textEdit_d133->toPlainText().toDouble());
db13.push_back(ui_->textEdit_d134->toPlainText().toDouble());
db13.push_back(ui_->textEdit_d135->toPlainText().toDouble());
db13.push_back(ui_->textEdit_d136->toPlainText().toDouble());

//set pmt14 gain value
ui_->textEdit_d141->append(ID[14]);
ui_->textEdit_d142->append(QString::number(gain_value[14][0],'g',3));
ui_->textEdit_d143->append(QString::number(gain_value[14][1],'g',3));
ui_->textEdit_d144->append(QString::number((int)gain_value[14][2]));
ui_->textEdit_d145->append(QString::number((int)gain_value[14][3]));
ui_->textEdit_d146->append(QString::number((int)DCR[14]));
db14.clear();
//db14.push_back(ui_->textEdit_d141->toPlainText().toDouble());
db14.push_back(ui_->textEdit_d142->toPlainText().toDouble());
db14.push_back(ui_->textEdit_d143->toPlainText().toDouble());
db14.push_back(ui_->textEdit_d144->toPlainText().toDouble());
db14.push_back(ui_->textEdit_d145->toPlainText().toDouble());
db14.push_back(ui_->textEdit_d146->toPlainText().toDouble());
//set pmt15 gain value
ui_->textEdit_d151->append(ID[15]);
ui_->textEdit_d152->append(QString::number(gain_value[15][0],'g',3));
ui_->textEdit_d153->append(QString::number(gain_value[15][1],'g',3));
ui_->textEdit_d154->append(QString::number((int)gain_value[15][2]));
ui_->textEdit_d155->append(QString::number((int)gain_value[15][3]));
ui_->textEdit_d156->append(QString::number((int)DCR[15]));
db15.clear();
//db15.push_back(ui_->textEdit_d151->toPlainText().toDouble());
db15.push_back(ui_->textEdit_d152->toPlainText().toDouble());
db15.push_back(ui_->textEdit_d153->toPlainText().toDouble());
db15.push_back(ui_->textEdit_d154->toPlainText().toDouble());
db15.push_back(ui_->textEdit_d155->toPlainText().toDouble());
db15.push_back(ui_->textEdit_d156->toPlainText().toDouble());

}














/*///////////////////////COMANDI MYSQL
CREATE DATABASE Augerpmt;

CREATE TABLE pmt (RUN varchar(10),ID varchar(10), p0 double, p1 double , HV1 double , HV2 double , DCR double, currentH double,  currentL double ,pdf_gain varchar(200) , pdf_linH varchar(200), pdf_linL varchar(200));

show databases;
use pmt;
show tables;
select * from pmt;
select ID from pmt;

INSERT INTO pmt (RUN, ID , p0 , p1 , HV1 , HV2) VALUES ('000','aaa' , 1.5 , 2e2 , 1450.4  , 129);

UPDATE pmt SET current = 155 WHERE id = 'aaa';

DELETE FROM pmt WHERE ID='1';
*/

