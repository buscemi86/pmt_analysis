#ifndef READ_FILE_H
#define READ_FILE_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <iomanip>  

#include <TMath.h>
#include <TTree.h>
#include <TFile.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TFrame.h>
#include <TF1.h>
#include <TH1F.h>
#include <TFitResult.h>
#include <QDir>
#include "RooPlot.h"
using namespace std;

class read_file{

public:
//read_file(const char*&, int&){};
read_file(){};
void read(QFileInfo file_data, int f);
double get_mean_A(){return mean_A; }
double get_sigma_A(){return sigma_A; }
double get_mean_Q(){return mean_Q; }
double get_sigma_Q(){return sigma_Q; }
TH1D* get_histo(){return histo_q;}
RooPlot* get_frame(){return xframe2;}

private:
//const char* filename;
int flag_fit;
TH1D* histo_q;
RooPlot* xframe;
RooPlot* xframe2;

double mean_A, sigma_A, mean_Q, sigma_Q;

};

#endif
