void gain_plot_2(const char* name, const char* name_histo)
{

TCanvas *c1 = new TCanvas();
c1->SetLogy();
gStyle->SetOptFit();

TFile* gain = new TFile(name);
//c1 = (TCanvas*) gain->Get("c1");
//c1->Draw();
TGraph* a;
//c1->cd(1);
 
a= (TGraph*) gain->Get("g1"); 
a->Draw("AP");


TCanvas *c2 = new TCanvas();
TFile* plot = new TFile(name_histo);
TH1D* b;
RooPlot* c;
c2->cd();
c2->SetLogy();
gStyle->SetOptFit();
c2->Divide(3,2);

c2->cd(1);
gPad->SetLogy();
b= (TH1D*) plot->Get("histo_1"); 
b->SetMarkerStyle(20);
b->SetTitle("1050V");
b->Draw("P");

c2->cd(2);
gPad->SetLogy();
b= (TH1D*) plot->Get("histo_2"); 
b->SetMarkerStyle(20);
b->SetTitle("1150V");
b->Draw("P");

c2->cd(3);
gPad->SetLogy();
b= (TH1D*) plot->Get("histo_3"); 
b->SetMarkerStyle(20);
b->SetTitle("1250V");
b->Draw("P");

c2->cd(4);
gPad->SetLogy();
b= (TH1D*) plot->Get("histo_4"); 
b->SetMarkerStyle(20);
b->SetTitle("1350V");
b->Draw("P");

c2->cd(5);
gPad->SetLogy();
b= (TH1D*) plot->Get("histo_5"); 
b->SetMarkerStyle(20);
b->SetTitle("1450V");
b->Draw("P");

c2->cd(6);
gPad->SetLogy();
c= (RooPlot*) plot->Get("xframe2");
c->SetTitle("SPE Spectrum");
c->Draw("P");


}
