#ifndef DATABASE_H
#define DATABASE_H

#include <iostream>
#include <QMainWindow>
#include <QSqlDatabase>
#include <vector>
#include "ui_mainwindow.h"
using namespace std;

namespace Ui {
class MainWindow;
}

class fill_db{
public:
fill_db(Ui::MainWindow *ui);

void show_data(vector<QString> , vector<vector<double>>,QString RunNumber="RUN_000");
void show_dcr(vector<QString> , vector<vector<double>>,QString RunNumber="RUN_000");
void store_all_data();

void store_data(vector<int>);

void load_data();

void clear();

int connect();

void disconnect();

void setDCR(vector<double> dcr){DCR=dcr;}
    private:
Ui::MainWindow *ui_;
QSqlDatabase db;
vector<QString> IDpmt;
vector<double> db0  , db1  , db2  , db3  , db4 ;
vector<double> db5  , db6  , db7  , db8  , db9 ;
vector<double> db10 , db11 , db12 , db13 , db14 , db15; 
vector<double> DCR;
QString RUN;
};

#endif
