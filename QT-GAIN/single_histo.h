#ifndef SINGLE_HISTO_H
#define SINGLE_HISTO_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <iomanip>  

#include <TMath.h>
#include <TTree.h>
#include <TFile.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TFrame.h>
#include <TF1.h>
#include <TH1F.h>
#include <TFitResult.h>

using namespace std;

class single_histo{

public:
int histo(string file_data, int f);

double get_mean_A(){return mean_A; }
double get_sigma_A(){return sigma_A; }
double get_mean_Q(){return mean_Q; }
double get_sigma_Q(){return sigma_A; }


private:
const char* filename;
int flag_fit;
//vector<double> vec_ch , vec_ampl;

double mean_A, sigma_A, mean_Q, sigma_Q;

};

#endif
