#include "write_pmt.h"


void write_pmt::analyze(vector<vector<QFileInfo>> vec){
cout<<"#######INFO: WRITE_PMT::ANALYZE#######"<<endl;

list_all_data.clear();
list=vec;
list_frame.clear();
read_file* read_data = new read_file();
vector<vector<double>> list_vec_data;
vector<TH1D*> vec_histo;
list_all_histo.clear();
for(unsigned int i=0;i<list.size();i++){
list_vec_data.clear();
vector<double> vec_data;
vec_histo.clear();
RooPlot* frame=new RooPlot();
	for(unsigned int j=0;j<list.at(i).size();j++){
		vec_data.clear();
		//cout<<list.at(i).at(j).filePath().toStdString()<<"  ";
		int flag;
		//if(list.at(i).at(j).fileName().contains("spe", Qt::CaseInsensitive)==true) flag=2;

		if(j==0) flag=2; //file spe è il primo della lista
		else flag=1;
//cout<<flag<<endl;
		read_data->read(list.at(i).at(j),flag);
		double ampl=read_data->get_mean_A();
		double sigmaA=read_data->get_sigma_A();
		double charge=read_data->get_mean_Q();
		double sigmaQ=read_data->get_sigma_Q();

//cout<<"*** "<<ampl<<"\t"<<sigmaA<<"\t"<<charge<<"\t"<<sigmaQ<<endl;

		vec_data.push_back(ampl*1000); // in mV
		vec_data.push_back(sigmaA*1000); // in mV
		vec_data.push_back(charge);
		vec_data.push_back(sigmaQ);
		
		list_vec_data.push_back(vec_data);
	        vec_histo.push_back(read_data->get_histo());
if(flag==2) frame=read_data->get_frame();						}

list_frame.push_back(frame);
list_all_data.push_back(list_vec_data);
list_all_histo.push_back(vec_histo);
					}	
delete read_data;
//cout<<list_all_data.at(1).at(1).size()<<" "<<list_all_data.size()<<" "<<list_all_data.at(1).size()<<endl;		
}

///////////////
int write_pmt::save_file(vector<QString> ID){

cout<<"#######INFO: WRITE_PMT::SAVE_FILE#######"<<endl;

//cout<<"ID size = "<<ID.size()<<" "<<list_all_data.at(1).at(1).size()<<" "<<list_all_data.size()<<" "<<list_all_data.at(1).size()<<endl;
const char* configfile ="config.dat";

QFile config(configfile);
QTextStream in(&config);
QString tmp, config_path;
if(!config.open(QIODevice::ReadOnly | QIODevice::Text)) cout<<"config file not found!"<<endl;
tmp = in.readLine();
config_path = in.readLine();


spe_ampl.clear();
for(unsigned int i=0;i<list_all_data.size();i++){

QString path=config_path;
QString index=ID[i];         
QString fff;
path.append("/");
path.append(index);
QString path_histo=path;
QDir dir;//("ddd");
//if(!dir.exists())
dir.mkpath(path); 
path.append("/gain_output.txt");
path_histo.append("/histo_output.root");
string path_str = path.toStdString();
ofstream myfile;

  	myfile.open (path_str.c_str());
myfile<<index.toStdString()<<internal<<setw(18) <<"Amp1 (mV) "<<internal<<setw(18)<<"sigma Amp1"<<internal<< setw(18)<<"Q1 (pVs)"<< setw(18)<<internal<<"sigma Q1"<< setw(18)<<endl;

spe_ampl.push_back(list_all_data.at(i).at(0).at(0));

	for(unsigned int j=0; j<list_all_data.at(i).size();j++) {
		
		for(int k=0;k<4;k++)  {
			myfile<<setiosflags(ios::fixed)<<setprecision(2)<<internal<<setw(18)<<fabs(list_all_data.at(i).at(j).at(k));}
			myfile <<endl;
//for(int k=0;k<4;k++) cout<<setiosflags(ios::fixed)<<setprecision(2)<<internal<<fabs(list_all_data.at(i).at(j).at(k))<<setw(18);
	//	cout<<endl;


}


 myfile.close(); 


TFile* file_root = new TFile(path_histo.toStdString().c_str(),"recreate");
if(file_root->IsOpen()){
TH1D* histo_1= new TH1D();
TH1D* histo_2= new TH1D();

histo_1->SetName("histo_1");
histo_2->SetName("histo_2");

histo_1->Write();
histo_2->Write();

//cout<<path_histo.toStdString().c_str()<<" /// "<< list_all_histo.at(i).size()<<endl;
//cout<<"SIZE = "<<list_all_histo.at(i).size()<<endl;
  		for(unsigned int w=0;w<list_all_histo.at(i).size();w++){
		string name;
	     
     if(w==0) name="histo_"+to_string(w);
	        
else if(w>0)	{
		if(list_all_histo.at(i).size()==6) name="histo_"+to_string(w+2);
		

		else if(list_all_histo.at(i).size()==8) name="histo_"+to_string(w);}


		file_root->cd();
		list_all_histo.at(i).at(w)->SetName(name.c_str());
		list_all_histo.at(i).at(w)->Write();
//delete &(*list_all_histo.at(i).at(w));
}
list_frame.at(i)->Write();
}

file_root->Write();
file_root->Close();

delete file_root;
}


return 0;
}









