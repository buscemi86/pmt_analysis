#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDesktopServices>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    ui->chooseButton->setEnabled(false);
    ui->analyzeButton->setEnabled(false);
    ui->analyze_histo->setEnabled(false);
    ui->clearButton->setEnabled(false);
    ui->storeAll_Button->setEnabled(false);
    ui->storeButton->setEnabled(false);
    ui->drawAllButton->setEnabled(false);
    ui->dcrButton->setEnabled(false);
    ui->lineEdit_psw->setEchoMode(QLineEdit::Password);
    ui->textEdit_dbStatus->clear();
    ui->textEdit_dbStatus->append("Disconnected");



config = new configuration(ui);

list = new mk_list();
db = new fill_db(ui);
dcr = new dark();

histogram = new single_histo();
flag_spe=0;
status_list=1;
status_dcr=1;
status_an=1;
status_histo=1;



}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_chooseButton_clicked()
{
 ui->textEdit_21->clear();
int flag_file=MainWindow::loadDataFiles(); //to choose directory of data-files to be analyzed 
if(flag_file==0){
status_list =list->setDir(Directory);
status_dcr = dcr->setDir(DCR_Dir);
if (status_list==0){
ui->label_28->setEnabled(true);
ui->analyzeButton->setEnabled(true);

status_list=1;		}
}
}

void MainWindow::on_analyzeButton_clicked()
{
MainWindow::enable_all_box();

pmt_data = new write_pmt();
vec_SPE.clear();
ID.clear();
db->clear();
ui->textEdit_21->clear();
gain_value.clear();
 ui->textEdit_21->append("In Progress");
MainWindow::delay();
ID=list->getID();
ui->analyzeButton->setEnabled(false);
pmt_data->analyze(list->get_list());

vector<vector<double>> vec_HV=list->getHV();

QFile HVfile("HV_set.dat");
HVfile.open(QIODevice::WriteOnly);
 QTextStream out(&HVfile); 
  

	for(unsigned int i=0;i<vec_HV.size();i++){
		if(i<10)	out <<"ch0"<<i<<" ";
		else out <<"ch"<<i<<" ";
			for(unsigned int j=0;j<vec_HV.at(i).size();j++)  out<<vec_HV.at(i).at(j)<<" ";//	cout<<vec_HV.at(i).at(j).toStdString()<<" "; 
			out<<endl;}

 HVfile.close();


status_an = pmt_data->save_file(ID);
if(status_an==0)
            {ui->textEdit_21->clear();
             ui->textEdit_21->append( "Processing data ");
	     ui->storeAll_Button->setEnabled(true);
	     ui->storeButton->setEnabled(true);
             status_an =0;}

else {ui->textEdit_21->clear();
ui->textEdit_21->append( "ERROR");
status_an=1;}
vec_SPE=pmt_data->getSPEampl();
//cout<<"STATUS= "<<status_an<<endl;

	if(status_an==0)	{
			for(unsigned int i=0; i<ID.size();i++)	{
				gain* plot_gain = new gain(ID[i], vec_HV[i]);
				gain_value.push_back(plot_gain->get_value());		
			//	MainWindow::delay();
				}	
			}


int threshold=10;
if( (fabs(vec_HV.at(0).at(0)- vec_HV.at(0).at(1))) > threshold) ui->label_4->setEnabled(false);
if( (fabs(vec_HV.at(1).at(0)- vec_HV.at(1).at(1))) > threshold) ui->label_6->setEnabled(false);
if( (fabs(vec_HV.at(2).at(0)- vec_HV.at(2).at(1))) > threshold) ui->label_7->setEnabled(false);
if( (fabs(vec_HV.at(3).at(0)- vec_HV.at(3).at(1))) > threshold) ui->label_8->setEnabled(false);
if( (fabs(vec_HV.at(4).at(0)- vec_HV.at(4).at(1))) > threshold) ui->label_9->setEnabled(false);
if( (fabs(vec_HV.at(5).at(0)- vec_HV.at(5).at(1))) > threshold) ui->label_10->setEnabled(false);
if( (fabs(vec_HV.at(6).at(0)- vec_HV.at(6).at(1))) > threshold) ui->label_11->setEnabled(false);
if( (fabs(vec_HV.at(7).at(0)- vec_HV.at(7).at(1))) > threshold) ui->label_12->setEnabled(false);
if( (fabs(vec_HV.at(8).at(0)- vec_HV.at(8).at(1))) > threshold) ui->label_5->setEnabled(false);
if( (fabs(vec_HV.at(9).at(0)- vec_HV.at(9).at(1))) > threshold) ui->label_13->setEnabled(false);
if( (fabs(vec_HV.at(10).at(0)- vec_HV.at(10).at(1))) > threshold) ui->label_14->setEnabled(false);
if( (fabs(vec_HV.at(11).at(0)- vec_HV.at(11).at(1))) > threshold) ui->label_15->setEnabled(false);
if( (fabs(vec_HV.at(12).at(0)- vec_HV.at(12).at(1))) > threshold) ui->label_16->setEnabled(false);
if( (fabs(vec_HV.at(13).at(0)- vec_HV.at(13).at(1))) > threshold) ui->label_17->setEnabled(false);
if( (fabs(vec_HV.at(14).at(0)- vec_HV.at(14).at(1))) > threshold) ui->label_18->setEnabled(false);
if( (fabs(vec_HV.at(15).at(0)- vec_HV.at(15).at(1))) > threshold) ui->label_19->setEnabled(false);

  db->show_data(ID,gain_value,RunNumber);
  ui->textEdit_21->clear();
  ui->textEdit_21->append( "Completed");
//
//status_an=1;
					
delete pmt_data;
ui->drawAllButton->setEnabled(true);
ui->clearButton->setEnabled(false);
if(status_dcr==0) ui->dcrButton->setEnabled(true);
cout<<"GAIN ANALYSIS COMPLETED"<<endl;
}







void MainWindow::on_drawButton_clicked()
{

const char* configfile ="config.dat";

string tmp  ;
ifstream config(configfile);

for(unsigned int i=0;i<14;i++)
getline(config,tmp);

double flag_spe = QString::fromStdString(tmp).toDouble();


 QString   IDdraw = QFileDialog::getExistingDirectory(this,
                   tr("Choose PMT Index"),
                   "../gain_output",
                  QFileDialog::DontResolveSymlinks);
ui->textEdit_22->clear();

QFileInfo file(IDdraw);
if (!IDdraw.isEmpty()){ ui->textEdit_22->append(file.fileName());

//gain* plot_gain = new gain(config->getSPEconfig(),config->getGAINconfig() , IDdraw);}
///////////////////
QString root_filename=IDdraw;
QString histo_filename=IDdraw;
root_filename.append("/gain.root");
histo_filename.append("/histo_output.root");
QProcess *process = new QProcess();
QString proc;
if(flag_spe==1) proc="./gain_plot_1.sh ";
else if(flag_spe==2) proc="./gain_plot_2.sh ";

proc.append(root_filename);
proc.append(" ");
proc.append(histo_filename);
cout<<proc.toStdString()<<endl;
process->start(proc);



}
}

void MainWindow::on_drawAllButton_clicked()
{
const char* configfile ="config.dat";

string tmp  ;
ifstream config(configfile);

getline(config,tmp); //1st line
getline(config,tmp); //2nd line
QString root_filename = QString::fromStdString(tmp);


//root_filename.append("/gain.root");
QProcess *process = new QProcess();
QString proc="./gain_plot_all.sh ";
proc.append(root_filename);
proc.append(" ");

for(unsigned int i=0; i<ID.size();i++) {proc.append(ID[i]);proc.append("/gain.root ");}
//cout<<endl;
//cout<<proc.toStdString()<<endl;
process->start(proc);
}


void MainWindow::on_choose_histo_clicked()
{
ui->textEdit_31->clear();
    fileName = QFileDialog::getOpenFileName(this,
        tr("Open Data File"), "../Data/", tr("Data Files (*)"));
ui->textEdit_30->clear();
ui->textEdit_30->clear();
QFileInfo file(fileName);
ui->textEdit_30->append( file.fileName());
ui->analyze_histo->setEnabled(true);
ui->label_43->setEnabled(true);

status_histo=1;
}

void MainWindow::on_analyze_histo_clicked()
{

ui->textEdit_31->clear();
ui->textEdit_31->append("In Progress");
MainWindow::delay();
cout<<fileName.toStdString()<<endl;
string name =fileName.toStdString();

    if(flag_spe==0)
         status_histo = histogram->histo(name,1);
    else status_histo = histogram->histo(name,2);


if(status_histo==0){    ui->textEdit_31->clear();
                  ui->textEdit_31->append( "Completed");
                  }
   
    else 	{     ui->textEdit_31->clear();
                  ui->textEdit_31->append( "ERROR");}

    QProcess* process = new QProcess();

    QString proc="root -l draw.C";
process->start(proc);
}
void MainWindow::on_checkBox_2_stateChanged(int arg1)
{
if(arg1==2) {flag_spe=1;}
else{ flag_spe=0;}
}





void MainWindow::on_checkBox_db0_stateChanged(int arg1)
{
if(arg1!=0) flag_db0=1;
else flag_db0=0;

}

void MainWindow::on_checkBox_db1_stateChanged(int arg1)
{
    if(arg1!=0) flag_db1=1;
    else flag_db1=0;
}

void MainWindow::on_checkBox_db2_stateChanged(int arg1)
{
    if(arg1!=0) flag_db2=1;
    else flag_db2=0;
}

void MainWindow::on_checkBox_db3_stateChanged(int arg1)
{
    if(arg1!=0) flag_db3=1;
    else flag_db3=0;
}

void MainWindow::on_checkBox_db4_stateChanged(int arg1)
{
    if(arg1!=0) flag_db4=1;
    else flag_db4=0;
}

void MainWindow::on_checkBox_db5_stateChanged(int arg1)
{
    if(arg1!=0) flag_db5=1;
    else flag_db5=0;
}

void MainWindow::on_checkBox_db6_stateChanged(int arg1)
{
    if(arg1!=0) flag_db6=1;
    else flag_db6=0;
}

void MainWindow::on_checkBox_db7_stateChanged(int arg1)
{
    if(arg1!=0) flag_db7=1;
    else flag_db7=0;
}

void MainWindow::on_checkBox_db8_stateChanged(int arg1)
{
    if(arg1!=0) flag_db8=1;
    else flag_db8=0;
}

void MainWindow::on_checkBox_db9_stateChanged(int arg1)
{
    if(arg1!=0) flag_db9=1;
    else flag_db9=0;
}

void MainWindow::on_checkBox_db10_stateChanged(int arg1)
{
    if(arg1!=0) flag_db10=1;
    else flag_db10=0;
}

void MainWindow::on_checkBox_db11_stateChanged(int arg1)
{
    if(arg1!=0) flag_db11=1;
    else flag_db11=0;
}

void MainWindow::on_checkBox_db12_stateChanged(int arg1)
{
    if(arg1!=0) flag_db12=1;
    else flag_db12=0;
}

void MainWindow::on_checkBox_db13_stateChanged(int arg1)
{
    if(arg1!=0) flag_db13=1;
    else flag_db13=0;
}

void MainWindow::on_checkBox_db14_stateChanged(int arg1)
{
    if(arg1!=0) flag_db14=1;
    else flag_db14=0;
}

void MainWindow::on_checkBox_db15_stateChanged(int arg1)
{
    if(arg1!=0) flag_db15=1;
    else flag_db15=0;
}



/*
void MainWindow::on_dbshowButton_clicked()
{

    db->show_data(ID,gain_value);
    ui->clearButton->setEnabled(true);
}
*/
void MainWindow::on_clearButton_clicked()
{
    db->clear();

}

void MainWindow::on_connectButton_clicked()
{
flag_db =    db->connect();  //1=connected

    if(flag_db==1) {  
			qDebug()<<"DB Connected";		   
			}
else qDebug()<<"Connection Failed";
}


void MainWindow::on_storeAll_Button_clicked(){
if(status_an==0 && flag_db==1) {  
					  db->store_all_data();
					qDebug()<<"All PMTs stored";}
else qDebug()<<"Connection Failed";
}


void MainWindow::on_uploadButton_clicked()
{
    db->load_data();
qDebug()<<"DB data loaded";
}

void MainWindow::on_disconnectButton_clicked()
{
   db->disconnect();
qDebug()<<"DB Disconnected";
}

void MainWindow::on_storeButton_clicked()
{
vector<int> vecDB;
vecDB.push_back(flag_db0);
vecDB.push_back(flag_db1);
vecDB.push_back(flag_db2);
vecDB.push_back(flag_db3);
vecDB.push_back(flag_db4);
vecDB.push_back(flag_db5);
vecDB.push_back(flag_db6);
vecDB.push_back(flag_db7);
vecDB.push_back(flag_db8);
vecDB.push_back(flag_db9);
vecDB.push_back(flag_db10);
vecDB.push_back(flag_db11);
vecDB.push_back(flag_db12);
vecDB.push_back(flag_db13);
vecDB.push_back(flag_db14);
vecDB.push_back(flag_db15);

//for(int i=0;i<vecDB.size();i++)cout<<vecDB[i]<<endl;

if(status_an==0 && flag_db==1) {db->store_data(vecDB);
				qDebug()<<"Selected PMTs stored";}

else qDebug()<<"Connection Failed";
}

void MainWindow::on_dcrButton_clicked()
{ if(status_dcr==0){
    ////////////////////DARK//////////
    dcr->clear_SPE();
    dcr->setSPE(vec_SPE);
    dcr->readDCR();
    db->setDCR(dcr->getDCR());
    db->clear();
    db->show_dcr(ID,gain_value,RunNumber);}
status_dcr=1;
    ui->dcrButton->setEnabled(false);
    ///////////////////
}
