#-------------------------------------------------
#
# Project created by QtCreator 2017-05-03T09:06:36
#
#-------------------------------------------------
QT_SELECT="default"
QTTOOLDIR="/opt/qt4/bin"
QTLIBDIR="/opt/qt4/bin"

QT       += core gui sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QTPLUGIN += QSQLMYSQL

TARGET = QT_GAIN
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11


INCLUDEPATH += $(ROOTSYS)/include $(ROOTSYS)/include/root
#LIBS += -L$(ROOTSYS)/lib -lCore  -lRIO -lNet \
#        -lHist -lGraf -lGraf3d -lGpad -lTree \
#        -lRint -lPostscript -lMatrix -lPhysics \
#        -lGui -lRGL    -lMathCore -lThread -pthread -lm -ldl -rdynamic

win32:LIBS += -L$(ROOTSYS)/lib -llibCore -llibCint -llibRIO -llibNet \
        -llibHist -llibGraf -llibGraf3d -llibGpad -llibTree \
        -llibRint -llibPostscript -llibMatrix -llibPhysics \
        -llibGui -llibRGL 



else:LIBS += -L$(ROOTSYS)/lib -L$(ROOTSYS)/lib/root  -lCore -lRIO -lNet \
        -lHist -lGraf -lGraf3d -lGpad -lTree \
        -lRint -lPostscript -lMatrix -lPhysics -lRooFit -lRooFitCore \
        -lGui -lMathCore -lThread -pthread -lm -ldl -rdynamic

SOURCES += main.cpp\
        *.cc
	 
HEADERS  += *.h\
	 
FORMS    += mainwindow.ui
